﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class GameServer : MonoBehaviour
    {
        int clientsInSessionCount;
        int clientsInitializedCount;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {

            //Initialize self
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.CLIENT_ENTERED_SESSION_MESSAGE, OnClientEnteredSession);
            MessagePump.instance.Register(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnClientEnteredSession(Message msg)
        {
            clientsInSessionCount++;

            if (PhotonController.instance.InARoom())
            {
                if (clientsInSessionCount == PhotonController.instance.GetRoomPlayerCount())
                {
                    MessagePump.instance.QueueMsg(new AllClientsEnteredSessionMessage());
                }
            }
            else
            {
                MessagePump.instance.QueueMsg(new AllClientsEnteredSessionMessage());
            }
        }

        private void OnClientInitialized(Message msg)
        {
            clientsInitializedCount++;

            if (PhotonController.instance.InARoom())
            {
                if (clientsInitializedCount == PhotonController.instance.GetRoomPlayerCount())
                {
                    MessagePump.instance.QueueMsg(new AllClientsInitializedMessage());
                }
            }
            else
            {
                MessagePump.instance.QueueMsg(new AllClientsInitializedMessage());
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_ENTERED_SESSION_MESSAGE, OnClientEnteredSession);
            MessagePump.instance.Unregister(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
        }
    }
}
