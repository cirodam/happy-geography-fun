﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class StandbyScoreController : MonoBehaviour
    {
        public GameSettings settings;

        Dictionary<string, int> clientScores;

        public int scoreRequired;

        public float timepFactor;
        public float distpFactor;

        public float scoreDistFactor;
        public float scoreTimeFactor;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {

            //Initialize self
            clientScores = new Dictionary<string, int>();
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            MessagePump.instance.Register(MessageType.CLIENT_SCORE_UPDATED_MESSAGE, OnClientScoreUpdated);
            MessagePump.instance.Register(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnClientInitialized(Message msg)
        {
            ClientInitializedMessage cMsg = (ClientInitializedMessage)msg;

            if (!clientScores.ContainsKey(cMsg.clientID))
            {
                clientScores.Add(cMsg.clientID, 0);
            }
        }

        private void OnPlayerLeftRoom(Message msg)
        {
            PlayerLeftRoomMessage cMsg = (PlayerLeftRoomMessage)msg;

            if (clientScores.ContainsKey(cMsg.player.UserId))
            {
                clientScores.Remove(cMsg.player.UserId);
            }
        }

        private void OnClientScoreUpdated(Message msg)
        {
            ClientScoreUpdatedMessage cMsg = (ClientScoreUpdatedMessage)msg;

            if (clientScores.ContainsKey(cMsg.clientID))
            {
                clientScores[cMsg.clientID] = cMsg.score;
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            MessagePump.instance.Unregister(MessageType.CLIENT_SCORE_UPDATED_MESSAGE, OnClientScoreUpdated);

            MessagePump.instance.Unregister(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
        }
    }
}
