﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class StandbyColorController : MonoBehaviour
    {
        //ColorList colorData;
        public List<Color> unallocated;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {

            //Initialize self

            //colorData = (ColorList)ScriptableObject.CreateInstance("ColorList");
            unallocated = new List<Color>();
            //foreach (Color c in colorData.getColors())
            //{
            //    unallocated.Add(c);
            //}

            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.CLIENT_COLOR_ASSIGNED_MESSAGE, OnClientColorAssigned);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnClientColorAssigned(Message msg)
        {
            ClientColorAssignedMessage cMsg = (ClientColorAssignedMessage)msg;

            if (unallocated.Contains(cMsg.getColor()))
            {
                unallocated.Remove(cMsg.getColor());
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_COLOR_ASSIGNED_MESSAGE, OnClientColorAssigned);
        }
    }
}
