﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace cirodam
{
    public class LevelController : MonoBehaviour
    {
        public GameSettings settings;
        public RoundController roundController;
        public ScoreController scoreController;

        List<PlacePos> unused;

        int currentRound;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Initialize self
            unused = new List<PlacePos>();

            PlacePos[] loaded = GameData.LoadPlaceData(SessionController.instance.sessionData.dataObject).places;
            foreach (PlacePos p in loaded)
            {
                unused.Add(p);
            }
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.GAME_STARTED_MESSAGE, OnGameStarted);
            yield return null;

            //Send messages
            yield return null;
        }

        public void OnRoundComplete()
        {
            if(unused.Count > 0)
            {
                StartRound();
            }
            else
            {
                MessagePump.instance.QueueMsg(new GameModeBeatenMessage());
            }
        }

        private void OnGameStarted(Message msg)
        {
            currentRound = 0;
            StartRound();
        }

        private void StartRound()
        {
            currentRound++;
            int index = Random.Range(0, unused.Count);
            PlacePos use = unused[index];
            unused.Remove(use);
            roundController.StartRound(currentRound, use);
        }

        public void InitFromStandby(StandbyLevelController standby)
        {
            currentRound = standby.currentRound;

            unused = standby.unused;
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.GAME_STARTED_MESSAGE, OnGameStarted);
        }
    }
}
