﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace cirodam
{
    public class StandbyLevelController : MonoBehaviour
    {

        public int currentLevel;
        public int currentRound;

        public List<PlacePos> unused;

        PlacePos[] loaded;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Initialize self
            unused = new List<PlacePos>();

            loaded = GameData.LoadPlaceData(SessionController.instance.sessionData.dataObject).places;
            foreach (PlacePos p in loaded)
            {
                unused.Add(p);
            }
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnRoundStarted(Message msg)
        {
            RoundStartedMessage cMsg = (RoundStartedMessage)msg;
            currentRound = cMsg.roundNumber;

            if (unused.Contains(cMsg.targetPlace))
            {
                unused.Remove(cMsg.targetPlace);
            }
            else if (unused.Count == 0)
            {
                loaded = GameData.LoadPlaceData(SessionController.instance.sessionData.dataObject).places;
                foreach (PlacePos p in loaded)
                {
                    unused.Add(p);
                }
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
        }
    }
}
