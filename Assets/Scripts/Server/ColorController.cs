﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class ColorController : MonoBehaviour
    {
        public List<Color> colors;

        List<Color> unallocated;
        List<Color> allocated;

        private void Awake()
        {
            unallocated = colors;
            allocated = new List<Color>();
        }

        private void Start()
        {
            MessagePump.instance.Register(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
        }

        private void OnClientInitialized(Message msg)
        {
            ClientInitializedMessage cMsg = (ClientInitializedMessage)msg;

            Color toAssign = unallocated[Random.Range(0, unallocated.Count)];
            unallocated.Remove(toAssign);
            
            allocated.Add(toAssign);
            MessagePump.instance.QueueMsg(new ClientColorAssignedMessage(cMsg.clientID, toAssign));
        }

        public void InitFromStandby(StandbyColorController standby)
        {
            unallocated = standby.unallocated;
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
        }
    }
}
