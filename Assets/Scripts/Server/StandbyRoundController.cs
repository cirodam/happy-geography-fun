﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class StandbyRoundController : MonoBehaviour
    {
        public Dictionary<string, ClientState> clients;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {

            //Initialize self
            clients = new Dictionary<string, ClientState>();
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            MessagePump.instance.Register(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessCompleted);
            MessagePump.instance.Register(MessageType.CLIENT_SHOWING_RESULT_COMPLETE_MESSAGE, OnClientShowResultsComplete);
            MessagePump.instance.Register(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnClientInitialized(Message msg)
        {
            ClientInitializedMessage cMsg = (ClientInitializedMessage)msg;

            if (!clients.ContainsKey(cMsg.clientID))
            {
                clients.Add(cMsg.clientID, ClientState.WAITING_TO_GUESS);
            }
        }

        private void OnClientGuessCompleted(Message msg)
        {
            ClientGuessCompleteMessage cMsg = (ClientGuessCompleteMessage)msg;
            if (clients.ContainsKey(cMsg.clientID))
            {
                clients[cMsg.clientID] = ClientState.WAITING_TO_SHOW_RESULTS;
            }
        }

        private void OnClientShowResultsComplete(Message msg)
        {
            ClientShowingResultCompleteMessage cMsg = (ClientShowingResultCompleteMessage)msg;
            if (clients.ContainsKey(cMsg.clientID))
            {
                clients[cMsg.clientID] = ClientState.WAITING_TO_GUESS;
            }
        }

        private void OnPlayerLeftRoom(Message msg)
        {
            PlayerLeftRoomMessage cMSg = (PlayerLeftRoomMessage)msg;

            if (clients.ContainsKey(cMSg.player.UserId))
            {
                clients.Remove(cMSg.player.UserId);
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            MessagePump.instance.Unregister(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessCompleted);
            MessagePump.instance.Unregister(MessageType.CLIENT_SHOWING_RESULT_COMPLETE_MESSAGE, OnClientShowResultsComplete);
        }
    }
}
