﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class RoundController : MonoBehaviour
    {
        public GameSettings settings;
        public LevelController levelController;

        public Dictionary<string, ClientState> clients;

        float elapsedTime;
        bool guessing;
        bool showing;
        bool paused;

        float guessingTimeout;
        float showingTimeout;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {

            //Initialize self
            clients = new Dictionary<string, ClientState>();

            guessingTimeout = 9.0f;
            showingTimeout = 9.0f;
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            MessagePump.instance.Register(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessCompleted);
            MessagePump.instance.Register(MessageType.CLIENT_SHOWING_RESULT_COMPLETE_MESSAGE, OnClientShowResultsComplete);
            MessagePump.instance.Register(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
            MessagePump.instance.Register(MessageType.GAME_PAUSED_MESSAGE, OnGamePaused);
            MessagePump.instance.Register(MessageType.GAME_RESUMED_MESSAGE, OnGameResumed);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnClientInitialized(Message msg)
        {
            ClientInitializedMessage cMsg = (ClientInitializedMessage)msg;

            if (!clients.ContainsKey(cMsg.clientID))
            {
                clients.Add(cMsg.clientID, ClientState.WAITING_TO_GUESS);
            }
        }

        private void OnClientGuessCompleted(Message msg)
        {
            ClientGuessCompleteMessage cMsg = (ClientGuessCompleteMessage)msg;
            if (clients.ContainsKey(cMsg.clientID))
            {
                clients[cMsg.clientID] = ClientState.WAITING_TO_SHOW_RESULTS;
                TryShowingState();
            }
        }

        private void OnClientShowResultsComplete(Message msg)
        {
            ClientShowingResultCompleteMessage cMsg = (ClientShowingResultCompleteMessage)msg;
            if (clients.ContainsKey(cMsg.clientID))
            {
                clients[cMsg.clientID] = ClientState.WAITING_TO_GUESS;
                TryRoundComplete();
            }
        }

        public void StartRound(int roundNum, PlacePos targetPlace)
        {
            MessagePump.instance.QueueMsg(new RoundStartedMessage(roundNum, targetPlace));
            elapsedTime = 0.0f;
            guessing = true;
        }

        private void Update()
        {
            if(!paused)
            {
                elapsedTime += Time.deltaTime;

                if(guessing && elapsedTime >= guessingTimeout)
                {

                }

                else if(showing && elapsedTime >= showingTimeout)
                {

                }

                //Find late clients
            }
        }

        private void OnGamePaused(Message msg)
        {
            paused = true;
        }

        private void OnGameResumed(Message msg)
        {
            paused = false;
        }

        private void OnPlayerLeftRoom(Message msg)
        {
            PlayerLeftRoomMessage cMSg = (PlayerLeftRoomMessage)msg;

            if (clients.ContainsKey(cMSg.player.UserId))
            {
                clients.Remove(cMSg.player.UserId);
            }

            if(guessing)
            {
                TryShowingState();
            }
            else if(showing)
            {
                TryRoundComplete();
            }
        }

        private void TryShowingState()
        {
            if (AllClientsHaveStatus(ClientState.WAITING_TO_SHOW_RESULTS))
            {
                elapsedTime = 0.0f;
                guessing = false;
                showing = true;
                MessagePump.instance.QueueMsg(new GuessingCompleteMessage());
            }
        }

        private void TryRoundComplete()
        {
            if (AllClientsHaveStatus(ClientState.WAITING_TO_GUESS))
            {
                showing = false;
                MessagePump.instance.QueueMsg(new RoundCompleteMessage());
                levelController.OnRoundComplete();
            }
        }

        private bool AllClientsHaveStatus(ClientState target)
        {
            int i = 0;
            foreach (ClientState status in clients.Values)
            {
                if (status != target)
                {
                    return false;
                }
                i++;
            }
            return true;
        }

        public void InitFromStandby(StandbyRoundController standby)
        {
            clients = standby.clients;
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            MessagePump.instance.Unregister(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessCompleted);
            MessagePump.instance.Unregister(MessageType.CLIENT_SHOWING_RESULT_COMPLETE_MESSAGE, OnClientShowResultsComplete);
            MessagePump.instance.Unregister(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
            MessagePump.instance.Unregister(MessageType.GAME_PAUSED_MESSAGE, OnGamePaused);
            MessagePump.instance.Unregister(MessageType.GAME_RESUMED_MESSAGE, OnGameResumed);
        }
    }
}
