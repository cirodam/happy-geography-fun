﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class ServerController : MonoBehaviour
    {
        public GameObject serverRoot;

        public Object GameServerPrefab;
        public Object StandbyServerPrefab;

        GameObject server;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {

            //Init Self
            StartServer();
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.ROOM_MASTER_CLIENT_CHANGED_MESSAGE, OnMasterClientChanged);

            yield return null;

            //Send messages
            yield return null;
        }

        public void StartServer()
        {
            if (PhotonController.instance.InARoom())
            {
                if (PhotonController.instance.IsMasterClient())
                {
                    server = (GameObject)Instantiate(GameServerPrefab);
                }
                else
                {
                    server = (GameObject)Instantiate(StandbyServerPrefab);
                }
            }
            else
            {
                server = (GameObject)Instantiate(GameServerPrefab);
            }

            server.transform.SetParent(serverRoot.transform);
        }

        private void OnMasterClientChanged(Message msg)
        {
            if (PhotonController.instance.IsMasterClient())
            {
                GameObject standby = server;

                GameObject loaded = (GameObject)Instantiate(GameServerPrefab);

                loaded.GetComponent<RoundController>().InitFromStandby(standby.GetComponent<StandbyRoundController>());
                loaded.GetComponent<LevelController>().InitFromStandby(standby.GetComponent<StandbyLevelController>());
                loaded.GetComponent<ScoreController>().InitFromStandby(standby.GetComponent<StandbyScoreController>());
                loaded.GetComponent<ColorController>().InitFromStandby(standby.GetComponent<StandbyColorController>());

                Destroy(standby);

                server = loaded;
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.ROOM_MASTER_CLIENT_CHANGED_MESSAGE, OnMasterClientChanged);
        }
    }
}

