﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class ScoreController : MonoBehaviour
    {
        public GameSettings settings;
        public LevelController levelController;

        Dictionary<string, int> clientScores;

        int scoreRequired;

        float timepFactor;
        float distpFactor;

        float scoreDistFactor;
        float scoreTimeFactor;
        float scoreDifficultyFactor = 20;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {

            //Initialize self
            clientScores = new Dictionary<string, int>();
            timepFactor = settings.startingTimepFactor;
            distpFactor = settings.startingDistpFactor;
            scoreDistFactor = settings.startingScoreDistFactor;
            scoreTimeFactor = settings.startingScoreTimeFactor;
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            MessagePump.instance.Register(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessComplete);
            MessagePump.instance.Register(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnPlayerLeftRoom(Message msg)
        {
            PlayerLeftRoomMessage cMsg = (PlayerLeftRoomMessage)msg;

            if (clientScores.ContainsKey(cMsg.player.UserId))
            {
                clientScores.Remove(cMsg.player.UserId);
            }
        }

        private void OnClientInitialized(Message msg)
        {
            ClientInitializedMessage cMsg = (ClientInitializedMessage)msg;

            if (!clientScores.ContainsKey(cMsg.clientID))
            {
                clientScores.Add(cMsg.clientID, 0);
            }
        }

        private void OnClientGuessComplete(Message msg)
        {
            ClientGuessCompleteMessage cMsg = (ClientGuessCompleteMessage)msg;

            //Calculate this clients new score
            if (clientScores.ContainsKey(cMsg.clientID))
            {
                int delta = (int)CalculateScore(cMsg.guessResult.distance, cMsg.guessResult.timeUsed, cMsg.guessResult.place.difficulty);
                clientScores[cMsg.clientID] += delta;
                MessagePump.instance.QueueMsg(new ClientScoreUpdatedMessage(cMsg.clientID, clientScores[cMsg.clientID], delta));
            }
        }

        private float CalculateScore(float distance, float timeUsed, int difficulty)
        {
            float distPerc = (1000 - (distance * settings.kmPerUnit)) / 1000;
            float timePerc = (settings.roundTime - timeUsed) / settings.roundTime;

            float distCont = (Mathf.Pow(2, distPerc * 10));
            float timeCont = 2*(Mathf.Pow(2, timePerc * 10));

            if (timePerc > 0.0f && distPerc > 0.0f)
            {
                return (distCont + timeCont);
            }

            return 0.0f;
        }

        private float CalculateScoreFromPerc(float distPerc, float timePerc, int difficulty)
        {
            if (timePerc > 0.0f)
            {
                return Mathf.Pow(scoreDistFactor, distPerc) + Mathf.Pow(scoreTimeFactor, timePerc);
            }

            return 0.0f;
        }

        public void InitFromStandby(StandbyScoreController standby)
        {
            scoreRequired = standby.scoreRequired;
            timepFactor = standby.timepFactor;
            distpFactor = standby.distpFactor;
            scoreDistFactor = standby.scoreDistFactor;
            scoreTimeFactor = standby.scoreTimeFactor;
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            MessagePump.instance.Unregister(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessComplete);
            MessagePump.instance.Unregister(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
        }
    }
}
