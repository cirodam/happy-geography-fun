﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{

    public enum RoundStatus
    {
        ROUND_STATUS_GUESSING,
        ROUND_STATUS_ORBITING,
        ROUND_STATUS_DISPLAYING
    }

}
