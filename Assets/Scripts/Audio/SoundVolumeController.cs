﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class SoundVolumeController : MonoBehaviour
    {
        AudioSource sound;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            sound = gameObject.GetComponent<AudioSource>();
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.SOUND_VOLUME_SET_MESSAGE, OnSoundVolumeSet);
            yield return null;

            //Send messages
        }

        private void OnSoundVolumeSet(Message msg)
        {
            SoundVolumeSetMessage cMsg = (SoundVolumeSetMessage)msg;

            if(sound != null)
            {
                sound.volume = cMsg.soundVolume;
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.SOUND_VOLUME_SET_MESSAGE, OnSoundVolumeSet);
        }
    }
}

