﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class BackgroundMusicController : MonoBehaviour
    {
        public AudioSource music;
        public float divisor;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.MUSIC_VOLUME_SET_MESSAGE, OnMusicVolumeSet);
            yield return null;

            //Send messages
        }

        private void OnMusicVolumeSet(Message msg)
        {
            MusicVolumeSetMessage cMsg = (MusicVolumeSetMessage)msg;

            music.volume = cMsg.musicVolume / divisor;

            if(!music.isPlaying)
            {
                music.Play();
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.MUSIC_VOLUME_SET_MESSAGE, OnMusicVolumeSet);
        }
    }
}

