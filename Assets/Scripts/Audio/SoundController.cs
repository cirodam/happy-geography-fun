﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class SoundController : MonoBehaviour
    {
        public AudioSource buttonClick5;
        public AudioSource buttonClick8;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.SOUND_VOLUME_SET_MESSAGE, OnSoundVolumeSet);
            MessagePump.instance.Register(MessageType.UI_BUTTON_CLICKED_MESSAGE, OnUIButtonClicked);
            yield return null;

            //Send messages
        }

        private void OnSoundVolumeSet(Message msg)
        {
            SoundVolumeSetMessage cMsg = (SoundVolumeSetMessage)msg;

            buttonClick5.volume = cMsg.soundVolume;
            buttonClick8.volume = cMsg.soundVolume;
        }

        private void OnUIButtonClicked(Message msg)
        {
            UIButtonClickedMessage cMsg = (UIButtonClickedMessage)msg;

            if(cMsg.audioSource == 5)
            {
                buttonClick5.Play();
            }
            else if(cMsg.audioSource == 8)
            {
                buttonClick8.Play();
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.SOUND_VOLUME_SET_MESSAGE, OnSoundVolumeSet);
            MessagePump.instance.Unregister(MessageType.UI_BUTTON_CLICKED_MESSAGE, OnUIButtonClicked);
        }
    }
}

