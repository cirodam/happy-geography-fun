﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace cirodam
{
    public class SessionSoundListener : MonoBehaviour
    {
        public AudioSource onGuess;
        public AudioSource onAutoOrbitComplete;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessComplete);
            MessagePump.instance.Register(MessageType.AUTO_ORBIT_FINISHED_MESSAGE, OnAutoOrbitComplete);
            MessagePump.instance.Register(MessageType.SOUND_VOLUME_SET_MESSAGE, OnSoundVolumeSet);
            yield return null;

            //Send Messages
            yield return null;
        }

        private void OnClientGuessComplete(Message msg)
        {
            onGuess.Play();
        }

        private void OnAutoOrbitComplete(Message msg)
        {
            onAutoOrbitComplete.Play();
        }

        private void OnSoundVolumeSet(Message msg)
        {
            SoundVolumeSetMessage cMsg = (SoundVolumeSetMessage)msg;

            onGuess.volume = cMsg.soundVolume;
            onAutoOrbitComplete.volume = cMsg.soundVolume;
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessComplete);
            MessagePump.instance.Unregister(MessageType.AUTO_ORBIT_FINISHED_MESSAGE, OnAutoOrbitComplete);
            MessagePump.instance.Unregister(MessageType.SOUND_VOLUME_SET_MESSAGE, OnSoundVolumeSet);
        }
    }
}

