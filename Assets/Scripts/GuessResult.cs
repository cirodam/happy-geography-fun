﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{

    [System.Serializable]
    public class GuessResult
    {
        public PlacePos place;

        public SVector3 guessPos;
        public float distance;
        public float timeUsed;
    }

}
