﻿using PlayFab;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class OptionsPanelController : MonoBehaviour
    {

        public GameObject panel;
        string lastUI;

        public TMP_InputField usernameInput;
        public Button changeNameButton;
        public TextMeshProUGUI nameErrorText;
        public TMP_Dropdown resolutionDropdown;
        public Toggle fullscreenToggle;

        public Slider musicSlider;
        public Slider soundSlider;

        List<Resolution> resolutions;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self

            if (!PlayerPrefs.HasKey("MusicVolume"))
            {
                PlayerPrefs.SetFloat("MusicVolume", 0.5f);
            }

            if (!PlayerPrefs.HasKey("SoundVolume"))
            {
                PlayerPrefs.SetFloat("SoundVolume", 0.5f);
            }


            resolutions = new List<Resolution>();
            bool included = false;
            foreach(Resolution res in Screen.resolutions)
            {
                foreach(Resolution check in resolutions)
                {
                    if(res.width == check.width && res.height == check.height)
                    {
                        included = true;
                    }
                }

                if(!included)
                {
                    resolutions.Add(res);
                }
                included = false;
            }
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Register(MessageType.PLAYFAB_SET_DISPLAY_NAME_SUCCESS_MESSAGE, OnSetDisplayNameSuccess);
            MessagePump.instance.Register(MessageType.PLAYFAB_SET_DISPLAY_NAME_FAILURE_MESSAGE, OnSetDisplayNameFailure);
            yield return null;

            //Send messages
            MessagePump.instance.QueueMsg(new MusicVolumeSetMessage(PlayerPrefs.GetFloat("MusicVolume")));
            MessagePump.instance.QueueMsg(new SoundVolumeSetMessage(PlayerPrefs.GetFloat("SoundVolume")));
        }

        public void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if (cMsg.nextUI == "OptionsPanel")
            {
                lastUI = cMsg.prevUI;
                panel.SetActive(true);
                musicSlider.value = PlayerPrefs.GetFloat("MusicVolume");
                soundSlider.value = PlayerPrefs.GetFloat("SoundVolume");

                int val = 0;
                foreach (Resolution res in resolutions)
                {
                    resolutionDropdown.options.Add(new TMP_Dropdown.OptionData(res.width + " x " + res.height));
                    if (res.width == Screen.currentResolution.width && res.height == Screen.currentResolution.height)
                    {
                        resolutionDropdown.value = val;
                    }
                    val++;
                }
                fullscreenToggle.isOn = Screen.fullScreen;

                usernameInput.text = PhotonController.instance.GetName();
                usernameInput.placeholder.GetComponent<TextMeshProUGUI>().text = PhotonController.instance.GetName();
            }
            else
            {
                if(panel.activeSelf)
                {
                    nameErrorText.gameObject.SetActive(false);
                    panel.SetActive(false);
                }
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if(panel.activeSelf)
                {
                    MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("OptionsPanel", lastUI));
                }
            }
        }

        public void OnBackButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(5));
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("OptionsPanel", lastUI));
        }

        public void OnResetStatisticsButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            Category.ClearCategories();
        }

        public void OnSelectUsernameButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            PlayFabController.instance.SetDisplayName(usernameInput.text);
        }

        private void OnSetDisplayNameSuccess(Message msg)
        {
            if(panel.activeSelf)
            {
                nameErrorText.text = "Name Changed";
                nameErrorText.gameObject.SetActive(true);
            }
        }

        public void OnSoundSliderValueChanged()
        {
            PlayerPrefs.SetFloat("SoundVolume", soundSlider.value);
            MessagePump.instance.QueueMsg(new SoundVolumeSetMessage(PlayerPrefs.GetFloat("SoundVolume")));
        }

        public void OnMusicSliderValueChanged()
        {
            PlayerPrefs.SetFloat("MusicVolume", musicSlider.value);
            MessagePump.instance.QueueMsg(new MusicVolumeSetMessage(PlayerPrefs.GetFloat("MusicVolume")));
        }

        private void OnSetDisplayNameFailure(Message msg)
        {
            if(panel.activeSelf)
            {
                PlayFabSetDisplayNameFailureMessage cMsg = (PlayFabSetDisplayNameFailureMessage)msg;

                if (cMsg.error.Error == PlayFabErrorCode.NameNotAvailable)
                {
                    nameErrorText.text = "Username Unavailable";
                }
                else if (cMsg.error.Error == PlayFabErrorCode.ProfaneDisplayName)
                {
                    nameErrorText.text = "Username Profane";
                }

                usernameInput.text = PhotonController.instance.GetName();
                nameErrorText.gameObject.SetActive(true);
            }
        }

        public void OnResolutionDropdownChanged()
        {
            Screen.SetResolution(resolutions[resolutionDropdown.value].width, resolutions[resolutionDropdown.value].height, Screen.fullScreen);
        }

        public void OnFullscreenToggleChanged()
        {
            if(fullscreenToggle.isOn)
            {
                int tWidth = Screen.currentResolution.width;
                int tHeight = Screen.currentResolution.height;

                int val = 0;
                foreach (Resolution res in resolutions)
                {
                    if (res.width == tWidth && res.height == tHeight)
                    {
                        resolutionDropdown.value = val;
                        break;
                    }
                    val++;
                }
            }

            Screen.fullScreen = !Screen.fullScreen;
        }

        public void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
        }
    }
}
