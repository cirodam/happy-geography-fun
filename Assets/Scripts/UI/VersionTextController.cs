﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


namespace cirodam
{
    public class VersionTextController : MonoBehaviour
    {
        public TextMeshProUGUI versionText;

        private void Awake()
        {
            versionText.text = "V " + Application.version + "." + PygmyMonkey.AdvancedBuilder.AppParameters.Get.buildNumber; 
        }
    }
}
