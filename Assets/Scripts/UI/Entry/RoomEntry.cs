﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace cirodam
{
    public class RoomEntry : MonoBehaviour
    {
        public TextMeshProUGUI nameText;
        public TextMeshProUGUI hostText;
    }
}
