﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class LeaderBoardEntry : MonoBehaviour
    {
        public TextMeshProUGUI rankText;
        public TextMeshProUGUI nameText;
        public TextMeshProUGUI scoreText;

        public void Set(int newRank, string newName, int newScore)
        {
            rankText.text = newRank.ToString();
            nameText.text = newName;
            scoreText.text = newScore.ToString("n0");
        }

        public void SetColor(Color c)
        {
            rankText.color = c;
            nameText.color = c;
            scoreText.color = c;
        }
    }
}
