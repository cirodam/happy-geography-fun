﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class LobbyEntry : MonoBehaviour
    {
        public TextMeshProUGUI numText;
        public TextMeshProUGUI nameText;
        public TextMeshProUGUI countText;
        public TextMeshProUGUI accessText;
    }
}