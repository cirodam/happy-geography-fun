﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class ScoreEntry : MonoBehaviour
    {
        public GameObject deltaPanel;

        public GameObject activeBG;

        public TextMeshProUGUI nameText;
        public TextMeshProUGUI scoreText;
        public TextMeshProUGUI deltaText;

        public bool spectating;

        string playerName;
        int score;
        int deltaScore;

        public void SetColor(Color color)
        {
            nameText.color = color;
            scoreText.color = color;
            deltaText.color = color;
        }

        public void SetScore(int newScore)
        {
            score = newScore;
        }

        public void SetDeltaScore(int newDelta)
        {
            deltaScore = newDelta;
        }

        public void UpdateScoreText()
        {
            scoreText.text = score.ToString();
        }

        public void UpdateDeltaText()
        {
            deltaText.text = "+ " + deltaScore.ToString();
        }

        public void ShowEnabled()
        {
            activeBG.SetActive(true);
        }
    }
}
