﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class KickPanelController : MonoBehaviour
    {
        public GameObject panel;
        public GameObject friendEntryPrefab;

        public GameObject poolRoot;
        public GameObject listRoot;

        public GameObjectPool pool;
        public List<GameObject> list;

        List<Player> playersToRemove;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            pool = new GameObjectPool();
            pool.root = poolRoot;
            pool.Fill(friendEntryPrefab, 10);

            list = new List<GameObject>();
            playersToRemove = new List<Player>();
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            yield return null;

            //Send messages

        }

        public void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if (cMsg.nextUI == "KickPanel")
            {
                Show();
            }
            else
            {
                Hide();
            }
        }

        private void Show()
        {
            panel.SetActive(true);
          

            foreach (Player p in PhotonController.instance.GetPlayersInRoom().Values)
            {
                if(p.UserId != PhotonController.instance.GetID())
                {
                    GameObject g = pool.Use();
                    g.GetComponentInChildren<TextMeshProUGUI>().text = p.NickName;
                    g.GetComponentInChildren<Toggle>().onValueChanged.AddListener((bool val) => { OnToggleButton(val, p); });
                    g.transform.SetParent(listRoot.transform);
                    list.Add(g);
                }
            }
        }

        private void Hide()
        {
            panel.SetActive(false);

            foreach (GameObject g in list)
            {
                g.GetComponentInChildren<Toggle>().onValueChanged.RemoveAllListeners();
                pool.AddToPool(g);
            }

            list.Clear();
        }

        public void OnToggleButton(bool value, Player player)
        {
            if (value)
            {
                if (!playersToRemove.Contains(player))
                {
                    playersToRemove.Add(player);
                }
            }
            else
            {
                if (playersToRemove.Contains(player))
                {
                    playersToRemove.Remove(player);
                }
            }
        }

        public void OnKickButton()
        {
            foreach (Player p in playersToRemove)
            {
                MessagePump.instance.QueueMsg(new PlayerKickedFromRoomMessage(p.UserId));
            }
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("KickPanel", "RoomPanel"));
        }

        public void OnBackButton()
        {
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("KickPanel", "RoomPanel"));
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
        }
    }
}
