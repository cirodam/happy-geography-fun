﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace cirodam
{
    public class InvitePanelController : MonoBehaviour
    {
        //Uses Steam

        public GameObject panel;
        public GameObject friendEntryPrefab;

        public GameObject poolRoot;
        public GameObject listRoot;

        public GameObjectPool pool;
        public List<GameObject> list;

        List<PlayFab.ClientModels.FriendInfo> selectedFriends;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            pool = new GameObjectPool();
            pool.root = poolRoot;
            pool.Fill(friendEntryPrefab, 10);

            list = new List<GameObject>();
            selectedFriends = new List<PlayFab.ClientModels.FriendInfo>();
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            yield return null;

            //Send messages

        }

        public void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if (cMsg.nextUI == "InvitePanel")
            {
                Show();
            }
            else
            {
                Hide();
            }
        }

        private void OnPlayFabFriendsListReady(Message msg)
        {
            PlayFabFriendsListReadyMessage cMsg = (PlayFabFriendsListReadyMessage)msg;

            foreach(PlayFab.ClientModels.FriendInfo friend in cMsg.friends)
            {
                GameObject g = pool.Use();
                g.transform.SetParent(listRoot.transform, false);
                g.GetComponentInChildren<TextMeshProUGUI>().text = friend.TitleDisplayName;
                g.GetComponentInChildren<Toggle>().onValueChanged.AddListener((bool value) => { OnToggleButton(value, friend); });
                list.Add(g);
            }
        }

        private void Show()
        {
            panel.SetActive(true);
            PlayFabController.instance.GetFriends();
        }

        private void Hide()
        {
            panel.SetActive(false);

            foreach (GameObject g in list)
            {
                g.GetComponentInChildren<Toggle>().onValueChanged.RemoveAllListeners();
                pool.AddToPool(g);
            }

            list.Clear();
        }

        public void OnToggleButton(bool value, PlayFab.ClientModels.FriendInfo friend)
        {
            if (value)
            {
                if (!selectedFriends.Contains(friend))
                {
                    selectedFriends.Add(friend);
                }
            }
            else
            {
                if (selectedFriends.Contains(friend))
                {
                    selectedFriends.Remove(friend);
                }
            }
        }

        public void OnInviteButton()
        {
            //foreach (Steamworks.Friend f in selectedFriends)
            //{
            //    SteamController.instance.InviteFriend(f);
            //}
        }

        public void OnBackButton()
        {
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("InvitePanel", "RoomPanel"));
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
        }
    }
}
