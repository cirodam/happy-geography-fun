﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace cirodam
{
    public class HintPanelController : MonoBehaviour
    {
        public GameObject hintPanel;
        public TextMeshProUGUI hintText;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Initialize self
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
            MessagePump.instance.Register(MessageType.AUTO_ORBIT_FINISHED_MESSAGE, OnAutoOrbitFinished);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnRoundStarted(Message msg)
        {
            RoundStartedMessage cMsg = (RoundStartedMessage)msg;

            if(cMsg.targetPlace.hint != "")
            {
                hintText.text = cMsg.targetPlace.hint;
                hintPanel.SetActive(true);
            }
        }

        private void OnAutoOrbitFinished(Message msg)
        {
            if(hintPanel.activeSelf)
            {
                hintPanel.SetActive(false);
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
            MessagePump.instance.Unregister(MessageType.AUTO_ORBIT_FINISHED_MESSAGE, OnAutoOrbitFinished);
        }
    }
}
