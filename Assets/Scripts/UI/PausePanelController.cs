﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace cirodam
{
    public class PausePanelController : MonoBehaviour
    {
        public GameObject panel;
        public GameObject list;
        public GameObject alternate;

        public TextMeshProUGUI playerText;

        Dictionary<string, string> pausingPlayers;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            pausingPlayers = new Dictionary<string, string>();
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Register(MessageType.GAME_RESUMED_MESSAGE, OnGameResumed);
            MessagePump.instance.Register(MessageType.CLIENT_PAUSING_GAME_MESSAGE, OnClientPausingGame);
            MessagePump.instance.Register(MessageType.CLIENT_RESUMING_GAME_MESSAGE, OnClientResumingGame);
            yield return null;

            //Send messages
        }

        public void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if (cMsg.nextUI == "PausePanel")
            {
                panel.SetActive(true);
            }
            else
            {
                panel.SetActive(false);
            }
        }

        private void OnGameResumed(Message msg)
        {
            panel.SetActive(false);
        }

        private void OnClientPausingGame(Message msg)
        {
            panel.SetActive(true);

            ClientPausingGameMessage cMsg = (ClientPausingGameMessage)msg;
            if(PhotonController.instance.InARoom())
            {
                pausingPlayers.Add(cMsg.clientID, PhotonController.instance.GetNameFromID(cMsg.clientID));
            }
            else
            {
                pausingPlayers.Add(cMsg.clientID, PhotonController.instance.GetID());
            }

            if (cMsg.clientID == PhotonController.instance.GetID())
            {
                list.SetActive(true);
                alternate.SetActive(false);
            }
            else
            {
                if (!list.activeSelf && !alternate.activeSelf)
                {
                    alternate.SetActive(true);
                }
                SetPlayerText();
            }
        }

        private void OnClientResumingGame(Message msg)
        {
            ClientResumingGameMessage cMsg = (ClientResumingGameMessage)msg;
            pausingPlayers.Remove(cMsg.clientID);

            if (cMsg.clientID == PhotonController.instance.GetID())
            {
                alternate.SetActive(true);
                list.SetActive(false);
            }
            SetPlayerText();
        }

        private void SetPlayerText()
        {
            playerText.text = "Game Paused By ";

            bool first = true;
            foreach (string s in pausingPlayers.Values)
            {
                if (first)
                {
                    playerText.text += s;
                    first = false;
                }
                else
                {
                    playerText.text += (", " + s);
                }
            }
        }

        public void OnResumeButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            MessagePump.instance.QueueMsg(new ClientResumingGameMessage(PhotonController.instance.GetID()));
        }

        public void OnRestartButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            SessionController.instance.ReloadSession();
        }

        public void OnOptionsButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("PausePanel", "OptionsPanel"));
        }

        public void OnMainMenuButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            SessionController.instance.ToMainMenu();
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Unregister(MessageType.GAME_RESUMED_MESSAGE, OnGameResumed);
            MessagePump.instance.Unregister(MessageType.CLIENT_PAUSING_GAME_MESSAGE, OnClientPausingGame);
            MessagePump.instance.Unregister(MessageType.CLIENT_RESUMING_GAME_MESSAGE, OnClientResumingGame);
        }
    }
}
