﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class FindPanelController : MonoBehaviour
    {
        public ClientData clientData;
        public GameObject panel;

        public TextMeshProUGUI nameText;
        public TextMeshProUGUI roundText;
        public TextMeshProUGUI findText;

        int maxRounds;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            PlacePos[] loaded = GameData.LoadPlaceData(SessionController.instance.sessionData.dataObject).places;
            maxRounds = loaded.Length;
            nameText.text = "Game Mode: " + SessionController.instance.sessionData.dataName;
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
            yield return null;

            //Send messages
        }

        private void OnRoundStarted(Message msg)
        {
            RoundStartedMessage cMsg = (RoundStartedMessage)msg;

            roundText.text = "Round: " + cMsg.roundNumber + " / " + maxRounds;
            findText.text = "Find: " + cMsg.targetPlace.placeName;
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
        }
    }
}
