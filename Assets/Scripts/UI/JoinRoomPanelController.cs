﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using TMPro;
using UnityEngine;

namespace cirodam
{
    public class JoinRoomPanelController : MonoBehaviour
    {
        public GameObject panel;

        public TMP_InputField passInput;
        public GameObject noticeText;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            yield return null;

            //Send messages
            yield return null;
        }

        public void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if (cMsg.nextUI == "JoinRoomPanel")
            {
                Show();
            }
            else
            {
                Hide();
            }
        }

        private void Show()
        {
            panel.SetActive(true);
        }

        private void Hide()
        {
            panel.SetActive(false);
            noticeText.SetActive(false);
        }

        public void OnBackButton()
        {
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("JoinRoomPanel", "LobbyPanel"));
        }

        public void OnJoinButton()
        {
            LobbyPanelController.selectedEntry.CustomProperties.TryGetValue("pw", out object t);
            byte[] pw = (byte[])t;

            if (IsPasswordCorrect(passInput.text, pw))
            {
                PhotonController.instance.JoinRoom(LobbyPanelController.selectedEntry.Name);
                MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("JoinRoomPanel", "RoomPanel"));
            }
            else
            {
                noticeText.SetActive(true);
            }
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Return) && panel.activeSelf)
            {
                OnJoinButton();
            }
        }

        private bool IsPasswordCorrect(string target, byte[] pw)
        {
            byte[] unhashed = Encoding.Unicode.GetBytes(target);
            SHA256Managed sha256 = new SHA256Managed();
            byte[] hashed = sha256.ComputeHash(unhashed);

            if (pw.SequenceEqual(hashed)) 
            {
                return true;
            }
            return false;
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
        }
    }
}
