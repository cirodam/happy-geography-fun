﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace cirodam
{
    public class RoomPanelController : MonoBehaviour
    {
        public MainMenuData mainMenuData;
        public GameSettings settings;

        public GameObject lobbyEntryPrefab;
        GameData gameData;

        public GameObject panel;
        public TextMeshProUGUI countText;
        public TMP_Dropdown dropdown;

        public GameObject masterButtons;
        public GameObject memberButtons;

        public GameObject poolRoot;
        public GameObject listRoot;

        public Button kickButton;
        public Button startButton;

        GameObjectPool pool;
        List<GameObject> list;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            pool = new GameObjectPool();
            pool.root = poolRoot;
            pool.Fill(lobbyEntryPrefab, settings.maxRoomSize);
            list = new List<GameObject>();

            gameData = GameData.LoadGameData();
            foreach (GameEntry g in gameData.games)
            {
                dropdown.options.Add(new TMP_Dropdown.OptionData(g.gameName));
            }
            dropdown.RefreshShownValue();

            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Register(MessageType.ROOM_JOINED_MESSAGE, OnRoomJoined);
            MessagePump.instance.Register(MessageType.PLAYER_JOINED_ROOM_MESSAGE, OnPlayerJoinedRoom);
            MessagePump.instance.Register(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
            MessagePump.instance.Register(MessageType.ROOM_TARGET_CHANGED_MESSAGE, OnRoomTargetChanged);
            MessagePump.instance.Register(MessageType.ROOM_TO_SESSION_MESSAGE, OnRoomToSession);
            MessagePump.instance.Register(MessageType.ROOM_LEFT_MESSAGE, OnRoomLeft);
            MessagePump.instance.Register(MessageType.ROOM_MASTER_CLIENT_CHANGED_MESSAGE, OnRoomMasterClientChanged);
            yield return null;

            //Send messages
            yield return null;
        }

        /// <summary>
        /// MessageListener for UI_PANEL_TRANSITION_MESSAGE
        /// </summary>
        /// <param name="msg"></param>
        public void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if (cMsg.nextUI == "RoomPanel")
            {
                panel.SetActive(true);
                if (PhotonController.instance.InARoom())
                {
                    ShowList();
                    ConfigureButtons();
                }
            }
            else
            {
                panel.SetActive(false);
                HideList();
                masterButtons.SetActive(false);
                memberButtons.SetActive(false);
            }
        }

        /// <summary>
        /// MessageListener for ROOM_MASTER_CLIENT_CHANGED
        /// </summary>
        /// <param name="msg"></param>
        private void OnRoomMasterClientChanged(Message msg)
        {
            RoomMasterClientChangedMessage cMsg = (RoomMasterClientChangedMessage)msg;

            RefreshList();
            ConfigureButtons();
        }

        /// <summary>
        /// MessageListener for ROOM_JOINED_MESSAGE
        /// </summary>
        /// <param name="msg"></param>
        private void OnRoomJoined(Message msg)
        {
            countText.text = PhotonController.instance.GetRoomPlayerCount() + " / " + settings.maxRoomSize;
            ShowList();
            if (PhotonController.instance.IsMasterClient())
            {
                masterButtons.SetActive(true);
                dropdown.interactable = true;
                PhotonController.instance.SetRoomProperty("rt", dropdown.value);
            }
            else
            {
                memberButtons.SetActive(true);
            }
        }

        /// <summary>
        /// MessageListener for ROOM_LEFT_MESSAGE
        /// </summary>
        /// <param name="msg"></param>
        private void OnRoomLeft(Message msg)
        {
            if(panel.activeSelf)
            {
                MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("RoomPanel", "LobbyPanel"));
            }
        }

        /// <summary>
        /// MessageListener for PLAYER_JOINED_ROOM_MESSAGE
        /// </summary>
        /// <param name="msg"></param>
        private void OnPlayerJoinedRoom(Message msg)
        {
            countText.text = PhotonController.instance.GetRoomPlayerCount() + " / " + settings.maxRoomSize;
            if (PhotonController.instance.GetRoomPlayerCount() >= 2)
            {
                kickButton.interactable = true;
                startButton.interactable = true;
            }

            RefreshList();
        }

        /// <summary>
        /// MessageListener for PLAYER_LEFT_ROOM_MESSAGE
        /// </summary>
        /// <param name="msg"></param>
        private void OnPlayerLeftRoom(Message msg)
        {
            countText.text = PhotonController.instance.GetRoomPlayerCount() + " / " + settings.maxRoomSize;
            RefreshList();
        }

        /// <summary>
        /// MessageListener for ROOM_TARGET_CHANGED_MESSAGE
        /// </summary>
        /// <param name="msg"></param>
        private void OnRoomTargetChanged(Message msg)
        {
            RoomTargetChangedMessage cMsg = (RoomTargetChangedMessage)msg;

            dropdown.value = cMsg.target;
            dropdown.RefreshShownValue();
        }

        /// <summary>
        /// MessageListener for ROOM_TO_SESSION_MESSAGE
        /// </summary>
        /// <param name="msg"></param>
        private void OnRoomToSession(Message msg)
        {
            SessionController.instance.sessionData.sceneName = gameData.games[dropdown.value].sceneName;
            SessionController.instance.sessionData.dataObject = gameData.games[dropdown.value].gameSource;
            SessionController.instance.sessionData.dataName = gameData.games[dropdown.value].gameName;
            SessionController.instance.sessionData.leaderboardID = gameData.games[dropdown.value].leaderboardID;
            SessionController.instance.sessionData.leaderboardName = gameData.games[dropdown.value].leaderboardName;
            SessionController.instance.sessionData.numPlayers = PhotonController.instance.GetRoomPlayerCount();

            SessionController.instance.ToSession();
        }

        private void ConfigureButtons()
        {
            masterButtons.SetActive(false);
            memberButtons.SetActive(false);

            if (PhotonController.instance.IsMasterClient())
            {
                masterButtons.SetActive(true);

                if(PhotonController.instance.GetRoomPlayerCount() > 1)
                {
                    kickButton.interactable = true;
                    startButton.interactable = true;
                }
                else
                {
                    kickButton.interactable = false;
                    //startButton.interactable = false;
                }
            }
            else
            {
                memberButtons.SetActive(true);
            }
        }

        private void ShowList()
        {
            HideList();
            foreach (Player p in PhotonController.instance.GetPlayersInRoom().Values)
            {
                GameObject g = pool.Use();
                g.GetComponent<RoomEntry>().nameText.text = p.NickName;
                if (PhotonController.instance.IsPlayerMasterClient(p))
                {
                    g.GetComponent<RoomEntry>().hostText.gameObject.SetActive(true);
                    list.Insert(0, g);
                }
                else
                {
                    list.Add(g);
                }
            }

            foreach(GameObject g in list)
            {
                g.transform.SetParent(listRoot.transform, false);
            }
        }

        private void HideList()
        {
            foreach (GameObject g in list)
            {
                g.GetComponent<RoomEntry>().hostText.gameObject.SetActive(false);
                pool.AddToPool(g);
            }
            list.Clear();
        }

        private void RefreshList()
        {
            HideList();
            ShowList();
        }

        public void OnDropdownValueChanged()
        {
            if (PhotonController.instance.IsMasterClient())
            {
                PhotonController.instance.SetRoomProperty("rt", dropdown.value);
            }
        }

        public void OnBackButton()
        {
            PhotonController.instance.LeaveRoom();
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("RoomPanel", "LobbyPanel"));
        }

        public void OnKickButton()
        {
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("RoomPanel", "KickPanel"));
        }

        public void OnStartButton()
        {
            if (PhotonController.instance.IsMasterClient())
            {
                MessagePump.instance.QueueMsg(new RoomToSessionMessage());
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Unregister(MessageType.ROOM_JOINED_MESSAGE, OnRoomJoined);
            MessagePump.instance.Unregister(MessageType.PLAYER_JOINED_ROOM_MESSAGE, OnPlayerJoinedRoom);
            MessagePump.instance.Unregister(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
            MessagePump.instance.Unregister(MessageType.ROOM_TARGET_CHANGED_MESSAGE, OnRoomTargetChanged);
            MessagePump.instance.Unregister(MessageType.ROOM_TO_SESSION_MESSAGE, OnRoomToSession);
            MessagePump.instance.Unregister(MessageType.ROOM_LEFT_MESSAGE, OnRoomLeft);
            MessagePump.instance.Unregister(MessageType.ROOM_MASTER_CLIENT_CHANGED_MESSAGE, OnRoomMasterClientChanged);
        }
    }
}
