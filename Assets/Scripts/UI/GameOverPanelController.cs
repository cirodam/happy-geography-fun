﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class GameOverPanelController : MonoBehaviour
    {
        public GameObject titlePanel;
        public TextMeshProUGUI titleText;
        public GameObject switchButtons;

        public Toggle leaderboardToggle;
        public Toggle statisticsToggle;

        public GameObject SPHostButtons;
        public GameObject MPHostButtons;
        public GameObject lostButtons;

        public ClientData clientData;

        int currentScore;
        bool gameOver;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Initialize self
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.GAMEMODE_BEATEN_MESSAGE, OnGameModeBeaten);
            MessagePump.instance.Register(MessageType.CLIENT_SCORE_UPDATED_MESSAGE, OnClientScoreUpdated);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnClientScoreUpdated(Message msg)
        {
            ClientScoreUpdatedMessage cMsg = (ClientScoreUpdatedMessage)msg;

            if(cMsg.clientID == PhotonController.instance.GetID())
            {
                currentScore = cMsg.score;
            }
        }

        private void OnGameModeBeaten(Message msg)
        {
            titlePanel.SetActive(true);
            switchButtons.SetActive(true);
            LeaderboardController.instance.AddScoreToLeaderboard(SessionController.instance.sessionData.leaderboardID, currentScore);

            if (PhotonController.instance.InARoom())
            {
                if (PhotonController.instance.IsMasterClient())
                {
                    MPHostButtons.SetActive(true);
                }
                else
                {

                }
            }
            else
            {
                SPHostButtons.SetActive(true);
            }
        }

        public void OnLeaderboardToggle(bool value)
        {
            if (leaderboardToggle.isOn)
            {
                leaderboardToggle.enabled = false;
                statisticsToggle.isOn = false;
                MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("", "LeaderboardPanel"));
            }
            else
            {
                leaderboardToggle.enabled = true;
            }
        }

        public void OnStatisticsToggle(bool value)
        {
            if (statisticsToggle.isOn == true)
            {
                statisticsToggle.enabled = false;
                leaderboardToggle.isOn = false;
                MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("", "StatisticsPanel"));
            }
            else
            {
                statisticsToggle.enabled = true;
            }
        }

        public void OnSpectateButton()
        {
            Hide();
            MessagePump.instance.QueueMsg(new ClientSpectatingMessage(clientData.clientID));
        }

        public void OnMainMenuButton()
        {
            SessionController.instance.ToMainMenu();
        }

        public void OnLobbyButton()
        {
            SessionController.instance.ToLobby();
        }

        public void OnRestartButton()
        {
            SessionController.instance.ReloadSession();
        }

        public void Hide()
        {
            switchButtons.SetActive(false);
            SPHostButtons.SetActive(false);
            MPHostButtons.SetActive(false);
            lostButtons.SetActive(false);
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.GAMEMODE_BEATEN_MESSAGE, OnGameModeBeaten);
            MessagePump.instance.Unregister(MessageType.CLIENT_SCORE_UPDATED_MESSAGE, OnClientScoreUpdated);
        }
    }
}
