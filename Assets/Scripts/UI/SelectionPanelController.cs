﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace cirodam
{
    public class SelectionPanelController : MonoBehaviour
    {
        public GameData gameData;
        public GameObject gameEntryPrefab;

        public GameObject panel;
        public GameObject poolRoot;
        public GameObject listRoot;
        public Button startButton;

        GameObjectPool pool;
        List<GameObject> list;

        GameObject selectedButton;
        GameEntry selectedEntry;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            pool = new GameObjectPool();
            list = new List<GameObject>();
            gameData = GameData.LoadGameData();
            pool.root = poolRoot;
            pool.Fill(gameEntryPrefab, gameData.games.Length);
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            yield return null;

            //Send messages
        }

        public void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if (cMsg.nextUI == "SelectionPanel")
            {
                if(!panel.activeSelf)
                {
                    Show();
                }
            }
            else
            {
                Hide();
            }
        }

        private void Show()
        {
            panel.SetActive(true);
            startButton.interactable = false;

            foreach (GameEntry gameType in gameData.games)
            {
                GameObject button = pool.Use();
                button.transform.SetParent(listRoot.transform, false);
                button.GetComponentInChildren<TextMeshProUGUI>().text = gameType.gameName;
                button.GetComponentInChildren<Toggle>().onValueChanged.AddListener((bool val) => { OnToggleButton(val, button, gameType); });
                list.Add(button);
            }
        }

        private void Hide()
        {
            if (selectedButton != null)
            {
                selectedButton.GetComponentInChildren<Toggle>().isOn = false;
                selectedButton = null;
                selectedEntry = null;
                startButton.interactable = false;
            }

            panel.SetActive(false);

            foreach (GameObject g in list)
            {
                g.GetComponentInChildren<Toggle>().onValueChanged.RemoveAllListeners();
                pool.AddToPool(g);
            }
            list.Clear();
        }

        public void OnToggleButton(bool value, GameObject button, GameEntry dataEntry)
        {
            if (selectedButton != null)
            {
                if (selectedButton != button)
                {
                    selectedButton.GetComponentInChildren<Toggle>().isOn = false;
                }
                else
                {
                    selectedButton = null;
                    selectedEntry = null;
                    startButton.interactable = false;
                    return;
                }
            }

            selectedButton = button;
            selectedEntry = dataEntry;
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(5));
            startButton.interactable = true;

        }

        public void OnStartButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            SessionController.instance.sessionData.sessionStatus = SessionStatus.SESSION_STATUS_SINGLEPLAYER;

            SessionController.instance.sessionData.sceneName = selectedEntry.sceneName;
            SessionController.instance.sessionData.dataName = selectedEntry.gameName;
            SessionController.instance.sessionData.dataObject = selectedEntry.gameSource;
            SessionController.instance.sessionData.leaderboardName = selectedEntry.leaderboardName;
            SessionController.instance.sessionData.leaderboardID = selectedEntry.leaderboardID;
            SessionController.instance.sessionData.numPlayers = 1;

            SessionController.instance.ToSession();
        }

        public void OnBackButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(5));
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("SelectionPanel", "LeaderboardPanel"));
        }

        private void DestroyEverything()
        {
            foreach (GameObject g in list)
            {
                Destroy(g);
            }
            pool.Clear();
        }

        private void OnDestroy()
        {
            DestroyEverything();
            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
        }
    }
}
