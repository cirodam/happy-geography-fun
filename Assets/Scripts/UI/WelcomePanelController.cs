﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class WelcomePanelController : MonoBehaviour
    {
        public GameObject panel;

        public GameObject hostButton;
        public GameObject memberButton;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.CLIENT_ENTERED_SESSION_MESSAGE, OnClientEnteredSession);
            MessagePump.instance.Register(MessageType.GAME_STARTED_MESSAGE, OnGameStarted);
            MessagePump.instance.Register(MessageType.ALL_CLIENTS_INITIALIZED_MESSAGE, OnAllClientsInitialized);

            yield return null;

            //Send messages
        }

        private void OnClientEnteredSession(Message msg)
        {
            ClientEnteredSessionMessage cMsg = (ClientEnteredSessionMessage)msg;

            if (cMsg.clientID == PhotonController.instance.GetID())
            {
                panel.SetActive(true);
                if (PhotonController.instance.InARoom())
                {
                    if (PhotonController.instance.IsMasterClient())
                    {
                        hostButton.SetActive(true);
                        hostButton.GetComponent<Button>().enabled = false;
                    }
                    else
                    {
                        memberButton.SetActive(true);
                    }
                }
                else
                {
                    hostButton.SetActive(true);
                    hostButton.GetComponent<Button>().enabled = false;
                }
            }
        }

        private void OnGameStarted(Message msg)
        {
            panel.SetActive(false);
        }

        public void OnStartButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            MessagePump.instance.QueueMsg(new GameStartedMessage());
        }

        private void OnAllClientsInitialized(Message msg)
        {
            hostButton.GetComponent<Button>().enabled = true;
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.GAME_STARTED_MESSAGE, OnGameStarted);
            MessagePump.instance.Unregister(MessageType.CLIENT_ENTERED_SESSION_MESSAGE, OnClientEnteredSession);
            MessagePump.instance.Unregister(MessageType.ALL_CLIENTS_INITIALIZED_MESSAGE, OnAllClientsInitialized);
        }
    }
}
