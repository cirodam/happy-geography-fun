﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class ScorePanelController : MonoBehaviour
    {
        public GameObject panel;
        public Color lostColor;

        public Object scoreEntryPrefab;
        public GameObject scoreList;
        Dictionary<string, GameObject> scores;

        public GameObject activeHeader;
        public GameObject spectatingHeader;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            scores = new Dictionary<string, GameObject>();
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            MessagePump.instance.Register(MessageType.CLIENT_COLOR_ASSIGNED_MESSAGE, OnClientColorAssigned);
            MessagePump.instance.Register(MessageType.CLIENT_SCORE_UPDATED_MESSAGE, OnClientScoreUpdated);
            MessagePump.instance.Register(MessageType.GAME_STARTED_MESSAGE, OnGameStarted);
            MessagePump.instance.Register(MessageType.AUTO_ORBIT_FINISHED_MESSAGE, OnAutoOrbitFinished);
            MessagePump.instance.Register(MessageType.CLIENT_SHOWING_RESULT_COMPLETE_MESSAGE, OnClientShowingResultsComplete);
            MessagePump.instance.Register(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnGameStarted(Message msg)
        {
            foreach (string id in scores.Keys)
            {
                scores[id].GetComponent<ScoreEntry>().SetScore(0);
            }
        }

        private void OnClientInitialized(Message msg)
        {
            ClientInitializedMessage cMsg = (ClientInitializedMessage)msg;

            if (!scores.ContainsKey(cMsg.clientID))
            {
                scores.Add(cMsg.clientID, ((GameObject)Instantiate(scoreEntryPrefab)));
                scores[cMsg.clientID].transform.SetParent(scoreList.transform, false);

                scores[cMsg.clientID].GetComponent<ScoreEntry>().nameText.text = cMsg.clientName;

                spectatingHeader.gameObject.transform.SetAsLastSibling();
            }
        }

        private void OnPlayerLeftRoom(Message msg)
        {
            PlayerLeftRoomMessage cMsg = (PlayerLeftRoomMessage)msg;

            if (scores.ContainsKey(cMsg.player.UserId))
            {
                Destroy(scores[cMsg.player.UserId].gameObject);
                scores.Remove(cMsg.player.UserId);
            }
        }

        private void OnClientColorAssigned(Message msg)
        {
            ClientColorAssignedMessage cMsg = (ClientColorAssignedMessage)msg;

            if (scores.ContainsKey(cMsg.clientID))
            {
                scores[cMsg.clientID].GetComponent<ScoreEntry>().SetColor(cMsg.getColor());
            }
        }

        private void OnClientScoreUpdated(Message msg)
        {
            ClientScoreUpdatedMessage cMsg = (ClientScoreUpdatedMessage)msg;

            if (scores.ContainsKey(cMsg.clientID))
            {
                scores[cMsg.clientID].GetComponent<ScoreEntry>().SetScore(cMsg.score);
                scores[cMsg.clientID].GetComponent<ScoreEntry>().SetDeltaScore(cMsg.deltaScore);
            }
        }

        private void OnAutoOrbitFinished(Message msg)
        {
            foreach (string id in scores.Keys)
            {
                if (!scores[id].GetComponent<ScoreEntry>().spectating)
                {
                    scores[id].GetComponent<ScoreEntry>().UpdateDeltaText();
                    scores[id].GetComponent<ScoreEntry>().deltaPanel.SetActive(true);
                }
            }
        }

        private void OnClientShowingResultsComplete(Message msg)
        {
            foreach (string id in scores.Keys)
            {
                scores[id].GetComponent<ScoreEntry>().UpdateScoreText();
                scores[id].GetComponent<ScoreEntry>().deltaPanel.SetActive(false);
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            MessagePump.instance.Unregister(MessageType.CLIENT_COLOR_ASSIGNED_MESSAGE, OnClientColorAssigned);
            MessagePump.instance.Unregister(MessageType.CLIENT_SCORE_UPDATED_MESSAGE, OnClientScoreUpdated);
            MessagePump.instance.Unregister(MessageType.GAME_STARTED_MESSAGE, OnGameStarted);
            MessagePump.instance.Unregister(MessageType.AUTO_ORBIT_FINISHED_MESSAGE, OnAutoOrbitFinished);
            MessagePump.instance.Unregister(MessageType.CLIENT_SHOWING_RESULT_COMPLETE_MESSAGE, OnClientShowingResultsComplete);
            MessagePump.instance.Unregister(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
        }
    }
}
