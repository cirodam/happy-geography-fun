﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class ActionButtonController : MonoBehaviour
    {

        public GameObject spButton;
        public GameObject mpButton;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            yield return null;

            //Register for messages
            yield return null;

            //Send messages
            yield return null;
        }

        public void OnSPButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            if (PhotonController.instance.InARoom())
            {
                PhotonController.instance.LeaveRoom();
            }

            if (PhotonController.instance.InALobby())
            {
                PhotonController.instance.LeaveLobby();
            }

            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("LeaderboardPanel", "SelectionPanel"));
        }

        public void OnMPButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            if (PhotonController.instance.InARoom())
            {
                PhotonController.instance.LeaveRoom();
            }

            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("LeaderboardPanel", "LobbyPanel"));
        }
    }
}
