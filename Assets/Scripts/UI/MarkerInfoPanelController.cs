﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class MarkerInfoPanelController : MonoBehaviour
    {
        public GameObject panel;
        public TextMeshProUGUI nameText;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.MOUSE_ENTER_STAYMARKER_MESSAGE, OnMouseEnterStayMarker);
            MessagePump.instance.Register(MessageType.MOUSE_EXIT_STAYMARKER_MESSAGE, OnMouseExitStayMarker);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnMouseEnterStayMarker(Message msg)
        {
            MouseEnterStayMarkerMessage cMsg = (MouseEnterStayMarkerMessage)msg;

            nameText.text = cMsg.stayMarker.GetComponent<StayMarker>().placeName;
            panel.transform.position = Camera.main.WorldToScreenPoint(cMsg.stayMarker.transform.position);
            panel.SetActive(true);
        }

        private void OnMouseExitStayMarker(Message msg)
        {
            panel.SetActive(false);
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.MOUSE_ENTER_STAYMARKER_MESSAGE, OnMouseEnterStayMarker);
            MessagePump.instance.Unregister(MessageType.MOUSE_EXIT_STAYMARKER_MESSAGE, OnMouseExitStayMarker);
        }
    }
}

