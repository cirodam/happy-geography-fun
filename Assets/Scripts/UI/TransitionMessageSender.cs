﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class TransitionMessageSender : MonoBehaviour
    {
        public void SendPanelTransitionMessage(string targetID)
        {
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("", targetID));
        }
    }
}
