﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class ResultPanelController : MonoBehaviour
    {
        bool paused;

        public GameObject panel;
        public TextMeshProUGUI timeText;
        public TextMeshProUGUI distText;

        public GameSettings settings;
        public ClientData clientData;

        float timeRemaing = 0;

        private void Start()
        {
            MessagePump.instance.Register(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessComplete);
            MessagePump.instance.Register(MessageType.GAME_PAUSED_MESSAGE, OnGamePaused);
            MessagePump.instance.Register(MessageType.GAME_RESUMED_MESSAGE, OnGameResumed);
            paused = false;
        }

        private void OnClientGuessComplete(Message msg)
        {
            ClientGuessCompleteMessage cMsg = (ClientGuessCompleteMessage)msg;

            if (cMsg.clientID == clientData.clientID && !clientData.spectating)
            {
                timeText.text = cMsg.guessResult.timeUsed.ToString("F2") + " s";

                if (cMsg.guessResult.timeUsed != settings.roundTime)
                {
                    if (settings.distanceUnit == DistanceUnit.IMPERIAL)
                    {
                        distText.text = Mathf.Round(cMsg.guessResult.distance * settings.miPerUnit).ToString() + " miles";
                    }
                    else
                    {
                        distText.text = Mathf.Round(cMsg.guessResult.distance * settings.kmPerUnit).ToString() + " km";
                    }
                }
                else
                {
                    distText.text = "---";
                }
            }
        }

        private void OnGamePaused(Message msg)
        {
            paused = true;
        }

        private void OnGameResumed(Message msg)
        {
            paused = false;
        }

        public void OnOrbitingComplete()
        {
            panel.SetActive(true);
            StartCoroutine(ShowResults());
        }

        IEnumerator ShowResults()
        {
            timeRemaing = 3.0f;

            while (timeRemaing > 0)
            {
                if (!paused)
                {
                    timeRemaing -= Time.deltaTime;
                }
                yield return null;
            }

            panel.SetActive(false);
            MessagePump.instance.QueueMsg(new ClientShowingResultCompleteMessage(clientData.clientID));
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessComplete);
            MessagePump.instance.Unregister(MessageType.GAME_PAUSED_MESSAGE, OnGamePaused);
            MessagePump.instance.Unregister(MessageType.GAME_RESUMED_MESSAGE, OnGameResumed);
        }
    }
}
