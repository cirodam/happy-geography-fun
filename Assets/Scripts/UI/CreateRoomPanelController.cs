﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace cirodam
{
    public class CreateRoomPanelController : MonoBehaviour
    {
        public GameObject panel;

        public TMP_InputField nameInput;
        public TMP_InputField passInput;
        public TextMeshProUGUI noticeText;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Register(MessageType.ROOM_CREATED_MESSAGE, OnRoomCreated);
            MessagePump.instance.Register(MessageType.ROOM_CREATION_FAILED_MESSAGE, OnRoomCreationFailed);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if (cMsg.nextUI == "CreateRoomPanel")
            {
                Show();
            }
            else
            {
                Hide();
            }
        }

        private void OnRoomCreated(Message msg)
        {
            EncryptPassword();
            PhotonController.instance.SetRoomPropertiesListedInLobby(new string[] { "pw", "al" });
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("CreateRoomPanel", "RoomPanel"));
        }

        private void OnRoomCreationFailed(Message msg)
        {
            noticeText.text = "Room Name Unavailable";
            noticeText.gameObject.SetActive(true);
        }

        private void Show()
        {
            panel.SetActive(true);
            nameInput.text = PlayFabController.instance.GetDisplayName();
            nameInput.placeholder.GetComponent<TextMeshProUGUI>().text = PlayFabController.instance.GetDisplayName();
            nameInput.Select();
            nameInput.ActivateInputField();
        }

        private void Hide()
        {
            panel.SetActive(false);
            noticeText.gameObject.SetActive(false);
        }

        public void OnBackButton()
        {
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("CreateRoomPanel", "LobbyPanel"));
        }

        public void OnCreateButton()
        {
            PhotonController.instance.CreateRoom(nameInput.text);
            noticeText.text = "Creating Room";
            noticeText.gameObject.SetActive(true);
        }

        private void EncryptPassword()
        {
            if(passInput.text == "")
            {
                PhotonController.instance.SetRoomProperty("al", "Public");
            }
            else
            {
                string pw = passInput.text;

                byte[] unhashed = Encoding.Unicode.GetBytes(pw);

                SHA256Managed sha256 = new SHA256Managed();
                byte[] hashed = sha256.ComputeHash(unhashed);
                PhotonController.instance.SetRoomProperty("pw", hashed);
                PhotonController.instance.SetRoomProperty("al", "Private");
            }
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Tab) && panel.activeSelf)
            {
                if(nameInput.isFocused)
                {
                    passInput.Select();
                    passInput.ActivateInputField();
                    return;
                }

                if (passInput.isFocused)
                {
                    nameInput.Select();
                    nameInput.ActivateInputField();
                    return;
                }
            }

            if(Input.GetKeyDown(KeyCode.Return) && panel.activeSelf)
            {
                PhotonController.instance.CreateRoom(nameInput.text);
                noticeText.text = "Creating Room";
                noticeText.gameObject.SetActive(true);
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Unregister(MessageType.ROOM_CREATED_MESSAGE, OnRoomCreated);
            MessagePump.instance.Unregister(MessageType.ROOM_CREATION_FAILED_MESSAGE, OnRoomCreationFailed);
        }
    }
}
