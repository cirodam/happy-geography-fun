﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class SideButtonController : MonoBehaviour
    {

        string lastPanel;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if (cMsg.nextUI != "OptionsPanel" && cMsg.nextUI != "StatisticsPanel")
            {
                lastPanel = cMsg.nextUI;
            }
        }

        public void OnOptionsButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage(lastPanel, "OptionsPanel"));
        }

        public void OnStatisticsButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage(lastPanel, "StatisticsPanel"));
        }

        public void OnExitButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(8));
            Application.Quit();
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
        }
    }
}
