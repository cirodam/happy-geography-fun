﻿using PlayFab;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace cirodam
{
    public class UsernamePanelController : MonoBehaviour
    {
        public GameObject panel;

        public TMP_InputField input;
        public TMP_Text noticeText;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Register(MessageType.PLAYFAB_SET_DISPLAY_NAME_SUCCESS_MESSAGE, OnSetDisplayNameSuccess);
            MessagePump.instance.Register(MessageType.PLAYFAB_SET_DISPLAY_NAME_FAILURE_MESSAGE, OnSetDisplayNameFailure);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if (cMsg.nextUI == "UsernamePanel")
            {
                panel.SetActive(true);
                input.Select();
                input.ActivateInputField();
            }
            else
            {
                panel.SetActive(false);
                noticeText.gameObject.SetActive(false);
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Return) && panel.activeSelf)
            {
                OnSelectButton();
            }
        }

        public void OnSelectButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(5));
            PlayFabController.instance.SetDisplayName(input.text);
            noticeText.text = "Selecting...";
            noticeText.gameObject.SetActive(true);
        }

        private void OnSetDisplayNameSuccess(Message msg)
        {
            if (panel.activeSelf)
            {
                MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("UsernamePanel", "LeaderboardPanel"));
            }
        }

        private void OnSetDisplayNameFailure(Message msg)
        {
            PlayFabSetDisplayNameFailureMessage cMsg = (PlayFabSetDisplayNameFailureMessage)msg;

            if(cMsg.error.Error == PlayFabErrorCode.NameNotAvailable)
            {
                noticeText.text = "Username Unavailable";
            }
            else if(cMsg.error.Error == PlayFabErrorCode.ProfaneDisplayName)
            {
                noticeText.text = "Username Profane";
            }

            noticeText.gameObject.SetActive(true);
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Unregister(MessageType.PLAYFAB_SET_DISPLAY_NAME_SUCCESS_MESSAGE, OnSetDisplayNameSuccess);
            MessagePump.instance.Unregister(MessageType.PLAYFAB_SET_DISPLAY_NAME_FAILURE_MESSAGE, OnSetDisplayNameFailure);
        }
    }
}
