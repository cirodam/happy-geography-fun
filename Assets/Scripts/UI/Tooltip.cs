﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class Tooltip : MonoBehaviour
    {

        RectTransform rt;

        public void Show()
        {
            gameObject.SetActive(true);
            rt = gameObject.GetComponent<RectTransform>();
        }

        void Update()
        {
            if (rt != null)
            {
                //Do nothing
            }
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
