﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{

    public class DebugMenuController : MonoBehaviour
    {
        public Text debugText;

        private void Start()
        {
            MessagePump.instance.Register(MessageType.LOBBY_JOINED_MESSAGE, ShowMessage);
        }

        private void ShowMessage(Message msg)
        {
            debugText.text = msg.GetMessageType().ToString();
        }
    }
}
