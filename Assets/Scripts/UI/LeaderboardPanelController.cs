﻿using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace cirodam
{
    public class LeaderboardPanelController : MonoBehaviour
    {
        public GameObject panel;
        public GameObject poolRoot;
        public GameObject listRoot;

        public GameObject waitingNotice;
        public GameObject listContainer;
        public GameObject errorNotice;

        public TextMeshProUGUI subheaderText;
        public Color normalColor;
        public Color localColor;

        public GameObject leaderboardEntryPrefab;

        public GameData gameData;
        int currentLeaderboard;

        GameObjectPool pool;
        List<GameObject> list;

        bool connected;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            currentLeaderboard = 0;

            connected = true;
            pool = new GameObjectPool();
            pool.root = poolRoot;
            pool.Fill(leaderboardEntryPrefab, 100);
            list = new List<GameObject>();

            gameData = GameData.LoadGameData();
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Register(MessageType.LEADERBOARD_SCORES_READY_MESSAGE, OnLeaderboardScoresReady);
            MessagePump.instance.Register(MessageType.GAMEMODE_BEATEN_MESSAGE, OnGameModeBeaten);
            MessagePump.instance.Register(MessageType.SCORE_ADDED_TO_LEADERBOARD, OnScoreAddedToLeaderboard);
            MessagePump.instance.Register(MessageType.FAILED_TO_GET_LEADERBOARD_SCORES_MESSAGE, OnFailedToGetLeaderboardScores);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnGameModeBeaten(Message msg)
        {
            for (int i = 0; i < gameData.games.Length; i++)
            {
                GameEntry g = gameData.games[i];

                if (g.leaderboardID == SessionController.instance.sessionData.leaderboardID)
                {
                    currentLeaderboard = i;
                    break;
                }
            }

            panel.SetActive(true);
            ShowLoadingNotice();
        }

        private void OnScoreAddedToLeaderboard(Message msg)
        {
            RequestScores();
        }

        private void OnFailedToGetLeaderboardScores(Message msg)
        {
            StartCoroutine(CheckConnection());
        }

        /// <summary>
        /// MessageListener for UI_PANEL_TRANSITION_MESSAGE
        /// </summary>
        /// <param name="msg"></param>
        private void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if (cMsg.nextUI == "LeaderboardPanel")
            {
                Show();
            }
            else
            {
                Hide();
            }
        }

        /// <summary>
        /// MessageListener for LEADERBOARD_SCORES_READY_MESSAGE
        /// </summary>
        /// <param name="msg"></param>
        private void OnLeaderboardScoresReady(Message msg)
        {
            LeaderboardScoresReadyMessage cMsg = (LeaderboardScoresReadyMessage)msg;
            ShowScores(cMsg.scores);
        }

        private void Show()
        {
            panel.SetActive(true);
            RequestScores();
        }

        private void Hide()
        {
            panel.SetActive(false);

            foreach (GameObject g in list)
            {
                g.GetComponent<LeaderBoardEntry>().SetColor(normalColor);
                pool.AddToPool(g);
            }
            list.Clear();
        }

        private void ShowLoadingNotice()
        {
            waitingNotice.SetActive(true);
            listContainer.SetActive(false);
            errorNotice.SetActive(false);

            subheaderText.text = gameData.games[currentLeaderboard].leaderboardName;
        }

        private void ShowScores(List<PlayerLeaderboardEntry> scores)
        {
            listContainer.SetActive(true);
            waitingNotice.SetActive(false);
            errorNotice.SetActive(false);

            foreach (GameObject g in list)
            {
                g.GetComponent<LeaderBoardEntry>().SetColor(normalColor);
                pool.AddToPool(g);
            }
            list.Clear();

            foreach (PlayerLeaderboardEntry entry in scores)
            {
                GameObject go = pool.Use();
                go.transform.SetParent(listRoot.transform, false);
                go.GetComponent<LeaderBoardEntry>().Set(entry.Position + 1, entry.DisplayName, entry.StatValue);
                if (entry.PlayFabId == PlayFabController.instance.playerData.playFabID)
                {
                    go.GetComponent<LeaderBoardEntry>().SetColor(localColor);
                }
                list.Add(go);
            }
        }

        private void ShowErrorNotice()
        {
            errorNotice.SetActive(true);
            waitingNotice.SetActive(false);
            listContainer.SetActive(false);
        }

        public void OnNextButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(5));
            currentLeaderboard++;
            if (currentLeaderboard == gameData.games.Length)
            {
                currentLeaderboard = 0;
            }
            RequestScores();
        }

        public void OnPrevButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(5));
            currentLeaderboard--;
            if (currentLeaderboard == -1)
            {
                currentLeaderboard = gameData.games.Length - 1;
            }
            RequestScores();
        }

        private void DestroyGameObjects()
        {
            foreach (GameObject g in list)
            {
                Destroy(g);
            }
            pool.Clear();
        }

        private void OnDestroy()
        {
            DestroyGameObjects();

            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Unregister(MessageType.LEADERBOARD_SCORES_READY_MESSAGE, OnLeaderboardScoresReady);
            MessagePump.instance.Unregister(MessageType.GAMEMODE_BEATEN_MESSAGE, OnGameModeBeaten);
            MessagePump.instance.Unregister(MessageType.SCORE_ADDED_TO_LEADERBOARD, OnScoreAddedToLeaderboard);
            MessagePump.instance.Unregister(MessageType.FAILED_TO_GET_LEADERBOARD_SCORES_MESSAGE, OnFailedToGetLeaderboardScores);
        }

        private void RequestScores()
        {
            ShowLoadingNotice();
            LeaderboardController.instance.GetLeaderboardScores(gameData.games[currentLeaderboard].leaderboardID);
        }

        IEnumerator CheckConnection()
        {
            if(!connected)
            {
                yield break;
            }

            while (!PlayFabController.instance.IsPlayFabLoggedIn())
            {
                connected = false;
                yield return new WaitForSeconds(3);
                ShowErrorNotice();
            }

            connected = true;
            RequestScores();
        }
    }
}