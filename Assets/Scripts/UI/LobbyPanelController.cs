﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class LobbyPanelController : MonoBehaviour
    {
        public GameObject lobbyEntryPrefab;

        public GameObject panel;
        public GameObject poolRoot;
        public GameObject listRoot;

        public Button joinButton;
        public Button createButton;
        public TextMeshProUGUI countText;
        public TMP_InputField searchInput;

        public GameObject waitingNotice;
        public GameObject errorNotice;
        public GameObject listContainer;

        List<RoomInfo> roomFullList;
        List<RoomInfo> roomSearchList;

        private bool inError;

        GameObjectPool pool;
        List<GameObject> list;

        GameObject selectedButton;
        public static RoomInfo selectedEntry;

        private void Awake()
        {
            StartCoroutine(init());
        }

        IEnumerator init()
        {
            //Init self

            roomFullList   = new List<RoomInfo>();
            roomSearchList = new List<RoomInfo>();

            pool = new GameObjectPool();
            pool.root = poolRoot;
            pool.Fill(lobbyEntryPrefab, 40);
            list = new List<GameObject>();
            yield return null;

            //Register for messages

            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Register(MessageType.PLAYFAB_LOGIN_FAILURE_MESSAGE, OnPlayFabLoginFailure);
            MessagePump.instance.Register(MessageType.PHOTON_DISCONNECTED_MESSAGE, OnPhotonDisconnected);
            MessagePump.instance.Register(MessageType.PHOTON_CONNECTED_TO_MASTER_MESSAGE, OnPhotonConnectedToMaster);
            MessagePump.instance.Register(MessageType.LOBBY_JOINED_MESSAGE, OnLobbyJoined);
            MessagePump.instance.Register(MessageType.ROOM_LIST_UPDATE_MESSAGE, OnRoomListUpdate);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if(cMsg.nextUI == "LobbyPanel")
            {
                if (PhotonController.instance.GetPhotonReady())
                {
                    PhotonController.instance.JoinLobby();
                }
                panel.SetActive(true);
                if(!inError)
                {
                    waitingNotice.SetActive(true);
                }
            }
            else
            {
                selectedButton = null;
                selectedEntry = null;

                panel.SetActive(false);
                errorNotice.SetActive(false);
                waitingNotice.SetActive(false);
                listContainer.SetActive(false);

                createButton.interactable = false;
                joinButton.interactable = false;

                MoveListToPool();
            }
        }

        private void OnPlayFabLoginFailure(Message msg)
        {
            inError = true;
            errorNotice.SetActive(true);
            waitingNotice.SetActive(false);
            listContainer.SetActive(false);
        }

        private void OnPhotonDisconnected(Message msg)
        {
            inError = true;
            errorNotice.SetActive(true);
            waitingNotice.SetActive(false);
            listContainer.SetActive(false);
        }

        private void OnPhotonConnectedToMaster(Message msg)
        {
            inError = false;
            errorNotice.SetActive(false);
            waitingNotice.SetActive(true);
            if (panel.activeSelf)
            {
                PhotonController.instance.JoinLobby();
            }
        }

        private void OnLobbyJoined(Message msg)
        {
            createButton.interactable = true;

            waitingNotice.SetActive(false);
            listContainer.SetActive(true);
        }

        private void OnRoomListUpdate(Message msg)
        {
            RoomListUpdateMessage cMsg = (RoomListUpdateMessage)msg;
            roomFullList.Clear();

            foreach (RoomInfo info in cMsg.rooms)
            {
                if (info.IsOpen && info.IsVisible && !info.RemovedFromList)
                {
                    roomFullList.Add(info);
                }
            }

            countText.text = roomFullList.Count.ToString();
            FindSearchList();
        }

        public void OnSearchInputValueChanged()
        {
            FindSearchList();
        }

        public void OnToggleButton(bool value, GameObject button, RoomInfo r)
        {
            if(selectedButton != null)
            {
                if(button != selectedButton)
                {
                    selectedButton.GetComponentInChildren<Toggle>().isOn = false;
                }
                else
                {
                    selectedButton = null;
                    selectedEntry = null;
                    joinButton.interactable = false;
                    return;
                }             
            }
            else
            {
                selectedButton = button;
                selectedEntry = r;
                joinButton.interactable = true;
            }
        }

        public void OnBackButton()
        {
            if (PhotonController.instance.InALobby())
            {
                PhotonController.instance.LeaveLobby();
            }
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("LobbyPanel", "LeaderboardPanel"));
        }

        public void OnCreateButton()
        {
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("LobbyPanel", "CreateRoomPanel"));
        }

        public void OnJoinButton()
        {
            selectedEntry.CustomProperties.TryGetValue("al", out object t);
            string al = (string)t;

            if (al == "Public")
            {
                PhotonController.instance.JoinRoom(selectedEntry.Name);
                MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("LobbyPanel", "RoomPanel"));
            }
            else
            {
                MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("LobbyPanel", "JoinRoomPanel"));
            }
        }

        private void FindSearchList()
        {
            roomSearchList.Clear();
            foreach (RoomInfo room in roomFullList)
            {
                if (room.Name.Contains(searchInput.text))
                {
                    roomSearchList.Add(room);
                }
            }
            RefreshList();
        }

        private void RefreshList()
        {
            MoveListToPool();
            int mark = 0;
            foreach (RoomInfo r in roomSearchList)
            {
                mark++;
                object a;
                string access;
                r.CustomProperties.TryGetValue("al", out a);
                access = (string)a;

                GameObject g = pool.Use();
                g.transform.SetParent(listRoot.transform);
                g.GetComponent<LobbyEntry>().numText.text = mark.ToString();
                g.GetComponent<LobbyEntry>().nameText.text = r.Name;
                g.GetComponent<LobbyEntry>().countText.text = r.PlayerCount.ToString() + " / 8";
                g.GetComponent<LobbyEntry>().accessText.text = access;
                g.GetComponent<Toggle>().onValueChanged.AddListener((bool val) => { OnToggleButton(val, g, r); });
                list.Add(g);
            }
        }

        private void MoveListToPool()
        {
            foreach (GameObject g in list)
            {
                g.GetComponent<Toggle>().onValueChanged.RemoveAllListeners();
                g.GetComponent<Toggle>().isOn = false;
                pool.AddToPool(g);
            }
            list.Clear();
        }

        private void DestroyGameObjects()
        {
            foreach (GameObject g in list)
            {
                Destroy(g);
            }
            pool.Clear();
        }

        private void OnDestroy()
        {
            DestroyGameObjects();

            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            MessagePump.instance.Unregister(MessageType.PLAYFAB_LOGIN_FAILURE_MESSAGE, OnPlayFabLoginFailure);
            MessagePump.instance.Unregister(MessageType.PHOTON_DISCONNECTED_MESSAGE, OnPhotonDisconnected);
            MessagePump.instance.Unregister(MessageType.PHOTON_CONNECTED_TO_MASTER_MESSAGE, OnPhotonConnectedToMaster);
            MessagePump.instance.Unregister(MessageType.LOBBY_JOINED_MESSAGE, OnLobbyJoined);
            MessagePump.instance.Unregister(MessageType.ROOM_LIST_UPDATE_MESSAGE, OnRoomListUpdate);
        }
    }
}
