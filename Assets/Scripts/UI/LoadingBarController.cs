﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class LoadingBarController : MonoBehaviour
    {
        bool paused;

        public GameSettings settings;
        public ClientData clientData;

        public Slider loadingBar;
        bool loading;

        private void Start()
        {
            paused = false;
            MessagePump.instance.Register(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
            MessagePump.instance.Register(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessComplete);
            MessagePump.instance.Register(MessageType.GAME_PAUSED_MESSAGE, OnGamePaused);
            MessagePump.instance.Register(MessageType.GAME_RESUMED_MESSAGE, OnGameResumed);
        }

        private void OnRoundStarted(Message msg)
        {
            loadingBar.value = 0.0f;
            loading = true;
            if (gameObject.activeSelf)
            {
                StartCoroutine(LoadingBar());
            }
        }

        private void OnClientGuessComplete(Message msg)
        {
            ClientGuessCompleteMessage cMsg = (ClientGuessCompleteMessage)msg;

            if (cMsg.clientID == clientData.clientID)
            {
                loading = false;
            }
        }

        private void OnGamePaused(Message msg)
        {
            paused = true;
        }

        private void OnGameResumed(Message msg)
        {
            paused = false;
        }

        IEnumerator LoadingBar()
        {
            float displacedTime = 0.0f;

            while (loading)
            {
                if (!paused)
                {
                    displacedTime += Time.deltaTime;
                    float perc = displacedTime / settings.roundTime;
                    loadingBar.value = Mathf.Lerp(0, 1, perc);
                }
                yield return null;
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
            MessagePump.instance.Unregister(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessComplete);
            MessagePump.instance.Unregister(MessageType.GAME_PAUSED_MESSAGE, OnGamePaused);
            MessagePump.instance.Unregister(MessageType.GAME_RESUMED_MESSAGE, OnGameResumed);
        }
    }
}
