﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace cirodam
{
    public class StatisticsPanelController : MonoBehaviour
    {
        public GameObject panel;
        public GameObject list;
        public GameObject statEntryPrefab;
        public GameObject notice;

        List<GameObject> children;
        string lastUI;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            children = new List<GameObject>();
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
            yield return null;

            //Send messages
        }

        public void OnUIPanelTransition(Message msg)
        {
            UIPanelTransitionMessage cMsg = (UIPanelTransitionMessage)msg;

            if (cMsg.nextUI == "StatisticsPanel")
            {
                lastUI = cMsg.prevUI;
                if(!panel.activeSelf)
                {
                    panel.SetActive(true);
                    PopulateChildren();
                }
            }
            else
            {
                DestroyChildren();
                panel.SetActive(false);
            }
        }

        private void PopulateChildren()
        {
            List<Category> cats = Category.LoadCategoriesFromFile();

            if (cats.Count == 0)
            {
                notice.SetActive(true);
            }

            foreach (Category cat in cats)
            {
                GameObject go = (GameObject)Instantiate(statEntryPrefab);
                go.transform.SetParent(list.transform, false);
                go.GetComponent<StatisticsEntry>().nameText.text = cat.categoryName;
                go.GetComponent<StatisticsEntry>().percText.text = (cat.getValue()*100.0f).ToString("F1") + "%";
                go.GetComponent<StatisticsEntry>().slider.value = cat.getValue();

                children.Add(go);
            }
        }

        private void DestroyChildren()
        {
            foreach (GameObject g in children)
            {
                Destroy(g);
            }

            children.Clear();
            notice.SetActive(false);
        }

        public void OnBackButton()
        {
            MessagePump.instance.QueueMsg(new UIButtonClickedMessage(5));
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("StatisticsPanel", lastUI));
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.UI_PANEL_TRANSITION_MESSAGE, OnUIPanelTransition);
        }
    }
}



