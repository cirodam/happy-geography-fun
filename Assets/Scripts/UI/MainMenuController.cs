﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class MainMenuController : MonoBehaviour
    {
        string startingPanel;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            startingPanel = "LeaderboardPanel";
            yield return null;

            //Register for messages
            yield return null;

            //Send messages
            if(PhotonController.instance.InARoom())
            {
                startingPanel = "RoomPanel";
            }
            else
            {
                startingPanel = "LeaderboardPanel";
            }
            MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("", startingPanel));

            yield return null;
        }
    }
}
