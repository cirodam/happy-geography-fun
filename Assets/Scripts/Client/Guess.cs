﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class Guess
    {
        public PlacePos place;
        public float timeRemaining;
        public bool paused;
        public bool over;
        public GuessResult result;

        //Initialize a new searching round with the time allowed for this round
        //and the city that the player will be searching for
        public Guess(float startTime, PlacePos pos)
        {
            result = new GuessResult();

            paused = false;
            result.place = pos;
            result.timeUsed = startTime;
            timeRemaining = startTime;
            place = pos;
        }

        //Update the round's time remaining
        public void Update()
        {
            if (!paused)
            {
                if (timeRemaining > 0)
                {
                    timeRemaining -= Time.deltaTime;
                }
                else
                {
                    over = true;
                }
            }
        }

        //Calculate how far away the user's guess is from the actual city location
        public void onGuess(Vector3 cartPos)
        {
            SCoord placePos = new SCoord(100, place.latitude, place.longitude);
            SCoord guessPos = Util.CartesianToSpherical(cartPos);

            //distance between lats and longs
            double dLat = (Mathf.PI / 180) * (placePos.latitude - guessPos.latitude);
            double dLong = (Mathf.PI / 180) * (placePos.longitude - guessPos.longitude);

            //Convert to radians
            placePos.latitude = (Mathf.PI / 180) * placePos.latitude;
            guessPos.latitude = (Mathf.PI / 180) * guessPos.latitude;

            //Formulae
            double a = System.Math.Pow(System.Math.Sin(dLat / 2), 2) +
                       System.Math.Pow(System.Math.Sin(dLong / 2), 2) *
                       Mathf.Cos(placePos.latitude) * Mathf.Cos(guessPos.latitude);

            double c = 2 * System.Math.Asin(System.Math.Sqrt(a));
            double d = placePos.radius * c;

            over = true;

            //Fill result
            result.guessPos = SVector3.ToSVector3(cartPos);
            result.timeUsed -= timeRemaining;
            result.distance = (float)d;

        }

        public void OnPause()
        {
            paused = true;
        }

        public void OnResume()
        {
            paused = false;
        }

        //Check to see if this round has ended
        public bool isOver()
        {
            return over;
        }

        //Get this rounds results
        public GuessResult getResult()
        {
            return result;
        }
    }
}
