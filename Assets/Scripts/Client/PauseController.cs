﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{

    public class PauseController : MonoBehaviour
    {
        public ClientData clientData;
        bool localPaused;
        bool generalPaused;
        List<string> pausingPlayers;

        bool started;
        bool spectating;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            pausingPlayers = new List<string>();
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.CLIENT_PAUSING_GAME_MESSAGE, OnClientPauseGame);
            MessagePump.instance.Register(MessageType.CLIENT_RESUMING_GAME_MESSAGE, OnClientResumeGame);
            MessagePump.instance.Register(MessageType.GAME_STARTED_MESSAGE, OnGameStarted);
            MessagePump.instance.Register(MessageType.CLIENT_SPECTATING_MESSAGE, OnClientSpectating);
            MessagePump.instance.Register(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnGameStarted(Message msg)
        {
            started = true;
        }

        private void OnClientSpectating(Message msg)
        {
            ClientSpectatingMessage cMsg = (ClientSpectatingMessage)msg;

            if (cMsg.clientID == clientData.clientID)
            {
                spectating = true;
            }
        }

        private void OnPlayerLeftRoom(Message msg)
        {
            PlayerLeftRoomMessage cMsg = (PlayerLeftRoomMessage)msg;

            if (pausingPlayers.Contains(cMsg.player.UserId))
            {
                pausingPlayers.Remove(cMsg.player.UserId);
            }

            if (pausingPlayers.Count == 0)
            {
                generalPaused = false;
                MessagePump.instance.QueueMsg(new GameResumedMessage());
            }
        }

        private void OnClientPauseGame(Message msg)
        {
            ClientPausingGameMessage cMsg = (ClientPausingGameMessage)msg;

            if (!pausingPlayers.Contains(cMsg.clientID))
            {
                pausingPlayers.Add(cMsg.clientID);
            }

            if (!generalPaused)
            {
                generalPaused = true;
                MessagePump.instance.QueueMsg(new GamePausedMessage());
            }
        }

        private void OnClientResumeGame(Message msg)
        {
            ClientResumingGameMessage cMsg = (ClientResumingGameMessage)msg;

            if (pausingPlayers.Contains(cMsg.clientID))
            {
                pausingPlayers.Remove(cMsg.clientID);
            }

            if (pausingPlayers.Count == 0)
            {
                generalPaused = false;
                MessagePump.instance.QueueMsg(new GameResumedMessage());
            }

            if(cMsg.clientID == PhotonController.instance.GetID())
            {
                localPaused = false;
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (started && !spectating)
                {
                    if (localPaused)
                    {
                        localPaused = false;
                        MessagePump.instance.QueueMsg(new ClientResumingGameMessage(clientData.clientID));
                    }
                    else
                    {
                        localPaused = true;
                        MessagePump.instance.QueueMsg(new ClientPausingGameMessage(clientData.clientID));
                    }
                }
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_PAUSING_GAME_MESSAGE, OnClientPauseGame);
            MessagePump.instance.Unregister(MessageType.CLIENT_RESUMING_GAME_MESSAGE, OnClientResumeGame);
            MessagePump.instance.Unregister(MessageType.GAME_STARTED_MESSAGE, OnGameStarted);
            MessagePump.instance.Unregister(MessageType.CLIENT_SPECTATING_MESSAGE, OnClientSpectating);
            MessagePump.instance.Unregister(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
        }
    }
}

