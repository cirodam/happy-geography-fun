﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace cirodam
{
    public class GameClient : MonoBehaviour
    {
        public GameSettings settings;
        public ClientData clientData;

        ClientState state;
        Guess currentGuess;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Initialize self
            clientData.clientID = PhotonController.instance.GetID();
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.ALL_CLIENTS_ENTERED_SESSION_MESSAGE, OnAllClientsEnteredSession);
            MessagePump.instance.Register(MessageType.GAME_PAUSED_MESSAGE, OnGamePaused);
            MessagePump.instance.Register(MessageType.GAME_RESUMED_MESSAGE, OnGameResumed);
            MessagePump.instance.Register(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
            yield return null;

            //Send messages
            MessagePump.instance.QueueMsg(new ClientEnteredSessionMessage(clientData.clientID));
            yield return null;
        }

        private void Update()
        {
            switch (state)
            {
                case ClientState.WAITING_TO_GUESS:
                    break;

                case ClientState.GUESSING:
                    currentGuess.Update();
                    if (currentGuess.isOver())
                    {
                        MessagePump.instance.QueueMsg(new ClientGuessCompleteMessage(clientData.clientID, currentGuess.getResult()));
                        state = ClientState.WAITING_TO_SHOW_RESULTS;
                    }
                    else if (Input.GetMouseButtonDown(1))
                    {
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                        if (Physics.Raycast(ray, out RaycastHit hit))
                            currentGuess.onGuess(hit.point);
                    }
                    break;

                case ClientState.WAITING_TO_SHOW_RESULTS:
                    break;

                case ClientState.SHOWING_RESULTS:
                    break;
            }
        }

        private void OnAllClientsEnteredSession(Message msg)
        {
            MessagePump.instance.QueueMsg(new ClientInitializedMessage(clientData.clientID, PhotonController.instance.GetName()));
        }

        private void OnRoundStarted(Message msg)
        {
            RoundStartedMessage cMsg = (RoundStartedMessage)msg;
            currentGuess = new Guess(settings.roundTime, cMsg.targetPlace);
            state = ClientState.GUESSING;

        }

        private void OnGuessingComplete(Message msg)
        {
            //To Showing results
        }

        private void OnGamePaused(Message msg)
        {
            if (currentGuess != null)
            {
                currentGuess.OnPause();
            }
        }

        private void OnGameResumed(Message msg)
        {
            if (currentGuess != null)
            {
                currentGuess.OnResume();
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.ALL_CLIENTS_ENTERED_SESSION_MESSAGE, OnAllClientsEnteredSession);
            MessagePump.instance.Unregister(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
            MessagePump.instance.Unregister(MessageType.GAME_PAUSED_MESSAGE, OnGamePaused);
            MessagePump.instance.Unregister(MessageType.GAME_RESUMED_MESSAGE, OnGameResumed);
        }
    }
}
