﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class StayMarker : MonoBehaviour
    {
        public string placeName;

        private void OnMouseEnter()
        {
            MessagePump.instance.QueueMsg(new MouseEnterStayMarkerMessage(gameObject));
        }

        private void OnMouseExit()
        {
            MessagePump.instance.QueueMsg(new MouseExitStayMarkerMessage());
        }
    }
}

