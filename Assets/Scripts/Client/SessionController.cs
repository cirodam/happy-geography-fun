﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace cirodam
{

    public class SessionController : MonoBehaviour
    {
        public static SessionController instance;

        public string mainMenuScene;
        public SessionData sessionData;

        bool toLobby;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                StartCoroutine(Init());
            }
            else
            {
                Destroy(gameObject);
            }
        }

        IEnumerator Init()
        {

            //Init Self
            sessionData = new SessionData();
            mainMenuScene = "MainMenu";
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.CUSTOM_ID_READY_MESSAGE, OnCustomIDReady);
            MessagePump.instance.Register(MessageType.ROOM_TO_MAIN_MENU_MESSAGE, OnRoomToMainMenu);
            MessagePump.instance.Register(MessageType.RELOADING_SESSION_MESSAGE, OnReloadingSession);
            MessagePump.instance.Register(MessageType.PHOTON_DISCONNECTED_MESSAGE, OnPhotonDisconnected);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnCustomIDReady(Message msg)
        {
            
        }

        private void OnPhotonDisconnected(Message msg)
        {
            if(SceneManager.GetActiveScene().name == "Session")
            {
                ToMainMenu();
            }
        }

        public void ToSession()
        {
            SceneManager.LoadScene(sessionData.sceneName, LoadSceneMode.Additive);
            SceneManager.UnloadSceneAsync("MainMenu");
        }

        public void ToMainMenu()
        {
            if (PhotonController.instance.InARoom())
            {
                PhotonController.instance.LeaveRoom();
            }
            SceneManager.LoadScene(mainMenuScene, LoadSceneMode.Additive);
            SceneManager.UnloadSceneAsync("Session");
        }

        //------------------------------
        public void ToLobby()
        {
            toLobby = true;
            MessagePump.instance.QueueMsg(new RoomToMainMenuMessage());
        }

        private void OnRoomToMainMenu(Message msg)
        {
            toLobby = true;
            SceneManager.LoadScene(mainMenuScene, LoadSceneMode.Additive);
            SceneManager.UnloadSceneAsync("Session");
        }
        //------------------------------------------

        public void ReloadSession()
        {
            MessagePump.instance.QueueMsg(new ReloadingSessionMessage());
        }

        private void OnReloadingSession(Message msg)
        {
            SceneManager.UnloadSceneAsync("Session");
            SceneManager.LoadScene("Session", LoadSceneMode.Additive);
        }

        //--------------------------------------------------

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CUSTOM_ID_READY_MESSAGE, OnCustomIDReady);
            MessagePump.instance.Unregister(MessageType.RELOADING_SESSION_MESSAGE, OnReloadingSession);
            MessagePump.instance.Unregister(MessageType.ROOM_TO_MAIN_MENU_MESSAGE, OnRoomToMainMenu);
            MessagePump.instance.Unregister(MessageType.PHOTON_DISCONNECTED_MESSAGE, OnPhotonDisconnected);
        }
    }
}
