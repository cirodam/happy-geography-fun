﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace cirodam
{
    public class StatisticsController : MonoBehaviour
    {
        public static StatisticsController instance;

        public GameSettings settings;

        public Dictionary<string, Category> categories;

        private void Awake()
        {

            categories = new Dictionary<string, Category>();

            if (instance == null)
            {
                instance = this;
                StartCoroutine(Init());
            }
            else
            {
                Destroy(gameObject);
            }
        }

        IEnumerator Init()
        {
            //Initialize self
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessComplete);
            MessagePump.instance.Register(MessageType.GAMEMODE_BEATEN_MESSAGE, OnGameModeBeaten);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnClientGuessComplete(Message msg)
        {
            ClientGuessCompleteMessage cMsg = (ClientGuessCompleteMessage)msg;

            string catName = cMsg.guessResult.place.catName;

            if (!categories.ContainsKey(catName))
            {
                categories.Add(catName, new Category(catName));
            }

            float add = cMsg.guessResult.distance*39.59f;

            if (add < 200)
            {
                categories[catName].numerator += (200-add)/200;
            }
            categories[catName].denominator++;
        }

        private void OnGameModeBeaten(Message msg)
        {
            List<Category> saved = Category.LoadCategoriesFromFile();

            foreach(Category c in saved)
            {
                if(!categories.ContainsKey(c.categoryName))
                {
                    categories.Add(c.categoryName, c);
                }
            }
            Category.SaveCategoriesToFile(categories.Values.ToList());
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessComplete);
            MessagePump.instance.Unregister(MessageType.GAMEMODE_BEATEN_MESSAGE, OnGameModeBeaten);
        }
    }
}