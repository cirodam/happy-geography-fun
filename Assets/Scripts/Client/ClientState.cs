﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public enum ClientState
    {
        WAITING_TO_GUESS,
        GUESSING,
        WAITING_TO_SHOW_RESULTS,
        SHOWING_RESULTS
    }
}
