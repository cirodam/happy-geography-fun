﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    enum MarkerMode
    {
        MARKER_MODE_SP,
        MARKER_MODE_MP
    }

    public class MarkerController : MonoBehaviour
    {
        public GameSettings settings;

        public GameObject markerPrefab;
        public GameObject stayMarkerPrefab;

        public Transform center;
        public GameObject markerRoot;

        public List<Color> sampleColors;
        Dictionary<string, Color> markerColors;

        GameObject targetMarker;
        Dictionary<string, GameObject> playerMarkers;
        List<GameObject> stayMarkers;

        public float errorCorrection;

        private void Awake()
        {
            StartCoroutine(Init());
        }

        IEnumerator Init()
        {
            //Init self
            targetMarker = Instantiate(markerPrefab);
            targetMarker.transform.SetParent(markerRoot.transform);

            markerColors  = new Dictionary<string, Color>();
            playerMarkers = new Dictionary<string, GameObject>();
            stayMarkers   = new List<GameObject>();
            yield return null;

            //Register for messages
            MessagePump.instance.Register(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            MessagePump.instance.Register(MessageType.CLIENT_COLOR_ASSIGNED_MESSAGE, OnClientColorAssigned);
            MessagePump.instance.Register(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessComplete);
            MessagePump.instance.Register(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
            MessagePump.instance.Register(MessageType.ROUND_COMPLETE_MESSAGE, OnRoundEnded);
            MessagePump.instance.Register(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
            MessagePump.instance.Register(MessageType.AUTO_ORBIT_FINISHED_MESSAGE, OnAutoOrbitFinished);
            yield return null;

            //Send messages
            yield return null;
        }

        private void OnPlayerLeftRoom(Message msg)
        {
            PlayerLeftRoomMessage cMsg = (PlayerLeftRoomMessage)msg;

            if (playerMarkers.ContainsKey(cMsg.player.UserId))
            {
                Destroy(playerMarkers[cMsg.player.UserId]);
                playerMarkers.Remove(cMsg.player.UserId);
            }
        }

        private void OnClientInitialized(Message msg)
        {
            ClientInitializedMessage cMsg = (ClientInitializedMessage)msg;

            if (!playerMarkers.ContainsKey(cMsg.clientID))
            {
                GameObject target = (GameObject)Instantiate(markerPrefab);
                playerMarkers.Add(cMsg.clientID, target);
                target.transform.SetParent(gameObject.transform);
            }
        }

        private void OnClientColorAssigned(Message msg)
        {
            ClientColorAssignedMessage cMsg = (ClientColorAssignedMessage)msg;

            if (playerMarkers.ContainsKey(cMsg.clientID))
            {
                playerMarkers[cMsg.clientID].GetComponent<Marker>().foundation.GetComponent<MeshRenderer>().material.color = cMsg.getColor();
            }
        }

        private void OnClientGuessComplete(Message msg)
        {
            ClientGuessCompleteMessage cMsg = (ClientGuessCompleteMessage)msg;

            GameObject guess = playerMarkers[cMsg.clientID];

            if (cMsg.guessResult.guessPos != null)
            {
                guess.SetActive(true);
                guess.transform.position = cMsg.guessResult.guessPos.toVector();
                guess.transform.LookAt(center);
                guess.transform.Translate(new Vector3(0.0f, 0.0f, -2.0f));
                guess.transform.up = -guess.transform.forward;
            }
        }

        private void OnRoundStarted(Message msg)
        {
            RoundStartedMessage cMsg = (RoundStartedMessage)msg;

            GameObject target = Instantiate(stayMarkerPrefab);
            target.transform.position = Util.SphericalToCartesian(new SCoord(settings.earthRadiusUnits, cMsg.targetPlace.latitude, cMsg.targetPlace.longitude));
            target.GetComponent<StayMarker>().placeName = cMsg.targetPlace.placeName;
            target.transform.LookAt(center);
            target.transform.up = -target.transform.forward;
            target.SetActive(false);
            target.transform.SetParent(markerRoot.transform);

            stayMarkers.Add(target);

            targetMarker.transform.position = Util.SphericalToCartesian(new SCoord(settings.earthRadiusUnits + 2, cMsg.targetPlace.latitude, cMsg.targetPlace.longitude));
            targetMarker.transform.LookAt(center);
            targetMarker.transform.up = -targetMarker.transform.forward;
            targetMarker.SetActive(false);
        }

        private void OnRoundEnded(Message msg)
        {
            foreach (GameObject g in playerMarkers.Values)
            {
                g.SetActive(false);
            }
            
        }

        private void OnAutoOrbitFinished(Message msg)
        {
            targetMarker.SetActive(true);
            stayMarkers[stayMarkers.Count - 1].SetActive(true);
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.CLIENT_INITIALIZED_MESSAGE, OnClientInitialized);
            MessagePump.instance.Unregister(MessageType.CLIENT_COLOR_ASSIGNED_MESSAGE, OnClientColorAssigned);
            MessagePump.instance.Unregister(MessageType.CLIENT_GUESS_COMPLETE_MESSAGE, OnClientGuessComplete);
            MessagePump.instance.Unregister(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
            MessagePump.instance.Unregister(MessageType.ROUND_COMPLETE_MESSAGE, OnRoundEnded);
            MessagePump.instance.Unregister(MessageType.PLAYER_LEFT_ROOM_MESSAGE, OnPlayerLeftRoom);
            MessagePump.instance.Unregister(MessageType.AUTO_ORBIT_FINISHED_MESSAGE, OnAutoOrbitFinished);
        }
    }
}
