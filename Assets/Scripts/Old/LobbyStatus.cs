﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LobbyStatus
{
    LOBBY_STATUS_NONE,
    LOBBY_STATUS_HOST,
    LOBBY_STATUS_MEMBER
}
