﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum ClientRoundStatus 
{
    GUESSING,
    WAITING_TO_SHOW_RESULTS,
    SHOWING_RESULTS,
    WAITING_TO_GUESS,
    GAME_LOST
}
