﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SessionStatus 
{
    SESSION_STATUS_SINGLEPLAYER,
    SESSION_STATUS_MULTIPLAYER,
}
