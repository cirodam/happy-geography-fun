﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SubjectNerd.Utilities;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/LeaderboardNameData", order = 1)]
public class LeaderboardNameData : ScriptableObject
{
    [Reorderable]
    public string[] leaderboards;
}
