﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RoundState 
{
    GUESSING,
    PAUSED_WHILE_GUESSING,
    PAUSED_WHILE_WAITING,
    WAITING
}
