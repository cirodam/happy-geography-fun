﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionController : MonoBehaviour
{/*
    public GameSettings settings;

    public static ConnectionController instance;

    private void Awake()
    {
        if(!instance)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            StartCoroutine(Init());
        }
        else
        {
            Destroy(gameObject);
        }
    }

    IEnumerator Init()
    {
        //Init self
        Config.ForUnity(Application.platform.ToString());
        InitClient();
        yield return null;

        //Register for messages
        yield return null;

        //Send messages
        yield return null;
    }

    private void InitClient()
    {
        if(Client.Instance != null)
        {
            Client.Instance.Dispose();
        }

        new Client(settings.appID);
    }

    public void TryConnection()
    {
        StartCoroutine(TryConnectionCoroutine());
    }

    IEnumerator TryConnectionCoroutine()
    {

        while(!Client.Instance.IsValid)
        {
            Debug.Log("Client Invalid");
            yield return new WaitForSeconds(5);

            if(Client.Instance.IsValid)
            {
                MessagePump.instance.QueueMsg(new ConnectionEstablishedMessage());
            }
        }
    }

    private void Update()
    { 
        if(Client.Instance != null)
        {
            Client.Instance.Update();
        }
    }

    private void OnApplicationQuit()
    {
        if (Client.Instance != null)
        {
            Client.Instance.Dispose();
        }
    }

    public string GetClientName()
    {
        if (Client.Instance != null)
        {
            return Client.Instance.Username;
        }
        else
        {
            return "Player 1";
        }
    }

    public ulong GetClientID()
    {
        if(Client.Instance != null)
        {
            return Client.Instance.SteamId;
        }
        else
        {
            return 001;
        }
    }

    public string GetNameForID(ulong memberID)
    {
        if (Client.Instance != null)
        {
            return Client.Instance.Friends.GetName(memberID);
        }
        else
        {
            return "Player 1";
        }
    }

 */
}
