﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LobbyLocation 
{
    LOCATION_LOBBY_SCREEN,
    LOCATION_IN_SESSION
}
