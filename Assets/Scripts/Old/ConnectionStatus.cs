﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ConnectionStatus 
{
    CONNECTED,
    DISCONNECTED
}
