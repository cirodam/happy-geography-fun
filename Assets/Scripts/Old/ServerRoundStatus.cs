﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ServerRoundStatus
{
    GUESSING,
    SHOWING_RESULTS
}
