﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Text;
using System;
using UnityEngine.SceneManagement;

/*
public class LobbyController : MonoBehaviour
{
    public MainMenuData mainMenuData;

    public static LobbyController instance;
    public GameSettings settings;

    public Dictionary<ulong, LobbyMember> lobby;
    public LobbyLocation lobbyLocation;
    public ulong hostID;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            StartCoroutine(Init());
        }
        else
        {
            Destroy(gameObject);
        }
    }

    IEnumerator Init()
    {

        //Initialize self
        lobby = new Dictionary<ulong, LobbyMember>();
        yield return null;

        //Reigster for messages
        yield return null;

        //Send messages
        yield return null;

        /*
        //Initialize things that require a connection
        if (Client.Instance != null)
        {
            Client.Instance.Lobby.OnLobbyJoined = OnLobbyJoined;
            Client.Instance.Lobby.OnLobbyCreated = OnLobbyCreated;
            Client.Instance.Lobby.OnLobbyStateChanged = OnLobbyStateChanged;

            Client.Instance.Networking.OnIncomingConnection = OnIncomingConnection;
            Client.Instance.Networking.OnConnectionFailed = OnConnectionFailed;
            Client.Instance.Networking.OnP2PData = OnP2PMessageRecieved;

            Client.Instance.Networking.SetListenChannel(0, true);
        }

    
    }

    //Try to create our own lobby
    public void CreateLobby()
    {
        if(Client.Instance != null)
        {
            Client.Instance.Lobby.Create(Lobby.Type.Public, settings.maxLobbySize);
        }
        else
        {
            MessagePump.instance.QueueMsg(new LobbyCreationFailedMessage());
        } 
    }

    //Try to leave our current lobby
    public void LeaveLobby()
    {
        if(Client.Instance != null)
        {
            Client.Instance.Lobby.Leave();
            lobby.Clear();
            MessagePump.instance.QueueMsg(new LobbyLeftMessage());
        } 
    }

    //Try to invite a friend to our lobby
    public void InvitePlayerToLobby(ulong playerID)
    {
        
        Client.Instance.Lobby.InviteUserToLobby(playerID);
        
    }

    public void ChangeLobbyLocation(LobbyLocation newLocation)
    {
        lobbyLocation = newLocation;
    }

    //We tried to create a lobby
    private void OnLobbyCreated(bool success)
    {
        
        if(success)
        {
            lobbyLocation = LobbyLocation.LOCATION_LOBBY_SCREEN;
            lobby.Add(Client.Instance.SteamId, new LobbyMember(Client.Instance.SteamId, Client.Instance.Username));
            hostID = Client.Instance.Lobby.Owner;
            MessagePump.instance.QueueMsg(new LobbyCreatedMessage(Client.Instance.SteamId));
        }
        else
        {
            MessagePump.instance.QueueMsg(new LobbyCreationFailedMessage());
        }
        
    }

    //We tried to join a lobby
    private void OnLobbyJoined(bool success)
    {
        
        if(success)
        {
            lobbyLocation = LobbyLocation.LOCATION_LOBBY_SCREEN;
            foreach (ulong member in Client.Instance.Lobby.GetMemberIDs())
            {
                if(!lobby.ContainsKey(member))
                {
                    lobby.Add(member, new LobbyMember(member, Client.Instance.Friends.GetName(member)));
                }
            }
            hostID = Client.Instance.Lobby.Owner;
            mainMenuData.startingPanel = "LobbyPanel";
            SceneManager.LoadScene("MainMenu");
            MessagePump.instance.QueueMsg(new LobbyJoinedMessage(Client.Instance.Lobby.GetMemberIDs()));
        }
    }

    //Something changed in our lobby
    
    private void OnLobbyStateChanged(Lobby.MemberStateChange stateChange, ulong affector, ulong affected)
    {
        switch(stateChange)
        {
            case Lobby.MemberStateChange.Entered:
                lobby.Add(affector, new LobbyMember(affector, Client.Instance.Friends.GetName(affector)));
                MessagePump.instance.QueueMsg(new PlayerEnteredLobbyMessage(affector));
                break;

            case Lobby.MemberStateChange.Left:
                lobby.Remove(affector);
                MessagePump.instance.QueueMsg(new PlayerLeftLobbyMessage(affector));
                if(affector == hostID)
                {
                    hostID = Client.Instance.Lobby.Owner;
                    MessagePump.instance.QueueMsg(new LobbyHostChangedMessage(hostID));
                }
                break;
        }
    }

    

    //Broadcast a message to the lobby
    public void BroadcastMessageToLobby(Message msg)
    {
        if(Client.Instance != null)
        {
            if (msg.originalSender == 0 && msg.localOnly == false)
            {
                msg.originalSender = Client.Instance.SteamId;

                foreach (ulong member in lobby.Keys)
                {
                    byte[] data = Util.SerializeToByteArray(msg);
                    if (member != Client.Instance.SteamId)
                    {
                        if (!Client.Instance.Networking.SendP2PPacket(member, data, data.Length))
                        {
                            Debug.Log("Failed to send Packet");
                        }
                    }
                }
            }
        }
    }

    //We recieved a message from the lobby
    private void OnP2PMessageRecieved(ulong ID, byte[] bytes, int length, int channel)
    {
        Message msg = Util.Deserialize<Message>(bytes);
        MessagePump.instance.QueueMsg(msg);
    }

    //Someone is trying to connect to us
    private bool OnIncomingConnection(ulong ID)
    {
        if(lobby.ContainsKey(ID))
        {
            return true;
        }

        return false;
    }

    //We failed to connect to someone
    
    private void OnConnectionFailed(ulong ID, Networking.SessionError error)
    {
        Debug.Log("Connection failed due To Error");
    }

    public ulong GetClientLobbyID()
    {
        //return Client.Instance.SteamId;
        return 0;
    }

    public ulong GetLobbyHostID()
    {
        //return Client.Instance.Lobby.Owner;
        return 0;
    }

    public bool IsHost()
    {
        //if(Client.Instance.Lobby.Owner == Client.Instance.SteamId)
        //{
        //    return true;
        //}

        return false;
    }

    public bool IsInLobby(ulong player)
    {
        if(lobby.ContainsKey(player))
        {
            return true;
        }
        return false;
    }

    public bool InALobby()
    {
        if(lobby.Count > 0)
        {
            return true;
        }

        return false;
    }

    
}
*/
