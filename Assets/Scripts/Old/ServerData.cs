﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ServerData", order = 1)]
public class ServerData : ScriptableObject
{
    public int currentRound;
    public int currentLevel;
    public int scoreRequired;
}
