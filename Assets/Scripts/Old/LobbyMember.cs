﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyMember
{
    public ulong memberID;
    public string name;

    public LobbyMember(ulong newMemberID, string newName)
    {
        memberID = newMemberID;
        name     = newName;
    }
}
