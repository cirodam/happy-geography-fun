﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public UnityEvent GameStartedEvent;
    public UnityEvent GamePausedEvent;
    public UnityEvent GameResumedEvent;
    public UnityEvent GameQuitEvent;

    bool paused;

    bool started;

    private void Awake()
    {
        paused = false;
        started = false;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            if(paused)
            {
                paused = false;
                GameResumedEvent.Invoke();
            }
            else
            {
                paused = true;
                GamePausedEvent.Invoke();
            }
        }

        if(Input.GetKeyDown(KeyCode.T))
        {
            if (!started)
            {
                GameStartedEvent.Invoke();
                started = true;
            }
        }
    }

    public void StartGame()
    {
        started = true;
        GameStartedEvent.Invoke();
    }

    public void QuitGame()
    {
        GameQuitEvent.Invoke();
        Application.Quit();
    }

    public void ToMainMenu()
    {
        GameQuitEvent.Invoke();
        SceneManager.LoadScene("MainMenu");
    }
}
