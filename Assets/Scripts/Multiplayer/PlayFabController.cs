﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine.SceneManagement;

namespace cirodam
{
    public class PlayFabController : MonoBehaviour
    {
        public static PlayFabController instance;
        public PlayerData playerData;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                StartCoroutine(Init());
            }
            else
            {
                Destroy(gameObject);
            }
        }

        IEnumerator Init()
        {
            //Init Self
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Additive);

            playerData = new PlayerData();
            yield return null;

            //Register For Messages
            MessagePump.instance.Register(MessageType.PHOTON_DISCONNECTED_MESSAGE, OnPhotonDisconnected);
            MessagePump.instance.Register(MessageType.STEAM_CLIENT_CREATED_MESSAGE, OnSteamClientCreated);
            yield return null;

            //Send messages
            yield return null;
        }

        /// <summary>
        /// MessageListener for STEAM_CLIENT_CREATED_MESSAGE
        /// </summary>
        /// <param name="msg"></param>
        private void OnSteamClientCreated(Message msg)
        {
            LoginWithSteam();
        }

        private void LoginWithSteam()
        {
            PlayFabClientAPI.LoginWithSteam(new LoginWithSteamRequest { CreateAccount = true, SteamTicket = SteamController.instance.GetSteamAuthTicket(), TitleId = "8D073" }, OnLoginWithSteamSuccess, OnLoginWithSteamFailure);
        }

        private void OnLoginWithSteamSuccess(LoginResult res)
        {
            playerData.playFabID = res.PlayFabId;

            SetDisplayName(SteamController.instance.GetSteamUsername());
            MessagePump.instance.QueueMsg(new PlayfabLoginSuccessMessage());
            RequestPhotonToken();
        }

        private void OnLoginWithSteamFailure(PlayFabError err)
        {
            Debug.Log(err);
            MessagePump.instance.QueueMsg(new PlayFabLoginFailureMessage());
            StartCoroutine(LoginFromFailure());
        }

        /// <summary>
        /// Queries PlayFab for a list of player's friends.
        /// Callback functions are OnGetfriendsSuccess and 
        /// OnGetFriendsFailure
        /// </summary>
        public void GetFriends()
        {
            PlayFabClientAPI.GetFriendsList(new GetFriendsListRequest { }, OnGetFriendsSuccess, OnGetFriendsFailure);
        }

        private void OnGetFriendsSuccess(GetFriendsListResult res)
        {
            MessagePump.instance.QueueMsg(new PlayFabFriendsListReadyMessage(res.Friends));
        }

        private void OnGetFriendsFailure(PlayFabError error)
        {

        }

        public string GetDisplayName()
        {
            return playerData.displayName;
        }

        public bool IsPlayFabLoggedIn()
        {
            return PlayFabClientAPI.IsClientLoggedIn();
        }

        private void GetPlayerProfile()
        {
            PlayFabClientAPI.GetPlayerProfile(new GetPlayerProfileRequest() { PlayFabId = playerData.playFabID }, OnGetProfileSuccess, OnGetProfileFailure);
        }

        private void OnGetProfileSuccess(GetPlayerProfileResult res)
        {
            playerData.displayName = res.PlayerProfile.DisplayName;
            if (playerData.displayName == null)
            {
                MessagePump.instance.QueueMsg(new UIPanelTransitionMessage("", "UsernamePanel"));
            }
        }

        private void OnGetProfileFailure(PlayFabError error)
        {
            Debug.Log("Failure getting player profile");
        }

        /// <summary>
        /// Used to change the PlayFab DisplayName of the current user. Callback
        /// functions are OnSetDisplayNameSuccess and OnSetDisplayNameFailure
        /// </summary>
        /// <param name="target">Player's new name</param>
        public void SetDisplayName(string target)
        {
            PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest { DisplayName = target }, OnSetDisplayNameSuccess, OnSetDisplayNameFailure);
        }

        private void OnSetDisplayNameSuccess(UpdateUserTitleDisplayNameResult res)
        {
            playerData.displayName = res.DisplayName;
            MessagePump.instance.QueueMsg(new PlayFabSetDisplayNameSuccessMessage(res.DisplayName));
        }

        private void OnSetDisplayNameFailure(PlayFabError error)
        {
            MessagePump.instance.QueueMsg(new PlayFabSetDisplayNameFailureMessage(error));
        }

        private void RequestPhotonToken()
        {
            PlayFabClientAPI.GetPhotonAuthenticationToken(new GetPhotonAuthenticationTokenRequest { PhotonApplicationId = PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime }, OnGetPhotonTokenSuccess, OnGetPhotonTokenFailure);
        }

        private void OnGetPhotonTokenSuccess(GetPhotonAuthenticationTokenResult res)
        {
            var customAuth = new AuthenticationValues { AuthType = CustomAuthenticationType.Custom };
            customAuth.AddAuthParameter("username", playerData.playFabID);
            customAuth.AddAuthParameter("token", res.PhotonCustomAuthenticationToken);
            PhotonNetwork.AuthValues = customAuth;
            PhotonNetwork.ConnectUsingSettings();
        }

        private void OnGetPhotonTokenFailure(PlayFabError error)
        {
            MessagePump.instance.QueueMsg(new PlayFabLoginFailureMessage());
        }

        private void OnPhotonDisconnected(Message msg)
        {
            StartCoroutine(LoginFromFailure());
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.PHOTON_DISCONNECTED_MESSAGE, OnPhotonDisconnected);
            MessagePump.instance.Unregister(MessageType.STEAM_CLIENT_CREATED_MESSAGE, OnSteamClientCreated);
        }

        IEnumerator LoginFromFailure()
        {
            while(!IsPlayFabLoggedIn())
            {
                yield return new WaitForSeconds(4);
                LoginWithSteam();
            }
        }
    }
}
