﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace cirodam
{
    public class CustomLoginController : MonoBehaviour
    {
        public static CustomLoginController instance;
        public string customID;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                StartCoroutine(Init());
            }
            else
            {
                Destroy(gameObject);
            }
        }

        IEnumerator Init()
        {
            //InitSelf
            customID = SystemInfo.deviceUniqueIdentifier;
            yield return null;

            //RegisterForMessages
            yield return null;

            //Send messages
            MessagePump.instance.QueueMsg(new CustomIDReadyMessage(customID));
            yield return null;
        }
    }
}
