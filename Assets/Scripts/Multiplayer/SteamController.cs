﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.SceneManagement;
using Steamworks.Data;

namespace cirodam
{
    public class SteamController : MonoBehaviour
    {
        public GameSettings settings;

        public static SteamController instance;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                StartCoroutine(Init());
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        IEnumerator Init()
        {
            //InitSelf
            InitClient();
            yield return null;

            //RegisterForMessages
            yield return null;

            //Send messages
            if (Steamworks.SteamClient.IsValid)
            {
                MessagePump.instance.QueueMsg(new SteamClientCreatedMessage());
            }
            else
            {
                Debug.Log("Failed to init steam");
                MessagePump.instance.QueueMsg(new FailedToCreateSteamClientMessage());
                InitFromFailure();
            }
            yield return null;
        }

        private void InitClient()
        {
            Steamworks.SteamClient.RestartAppIfNecessary(settings.appID);
            try
            {
                Steamworks.SteamClient.Init(settings.appID);
                //Steamworks.SteamFriends.OnGameRichPresenceJoinRequested += OnInvitedByFriend;
            }
            catch (System.Exception e)
            {
                Debug.Log("Exception initalizing Steam: " + e);
            }
        }

        private void Update()
        {
            Steamworks.SteamClient.RunCallbacks();
        }

        public string GetSteamAuthTicket()
        {
            if (Steamworks.SteamClient.IsValid)
            {
                Steamworks.AuthTicket ticket = Steamworks.SteamUser.GetAuthSessionTicket();

                StringBuilder sb = new StringBuilder();
                foreach (byte b in ticket.Data)
                {
                    sb.AppendFormat("{0:x2}", b);
                }

                return sb.ToString();
            }

            return null;
        }

        public List<Steamworks.Friend> GetActiveFriends()
        {
            List<Steamworks.Friend> ret = new List<Steamworks.Friend>();

            foreach (Steamworks.Friend f in Steamworks.SteamFriends.GetFriends())
            {
                if (f.IsOnline)
                {
                    ret.Add(f);
                }
            }

            return ret;
        }

        public void InviteFriend(Steamworks.Friend f)
        {
            f.InviteToGame("Spacewar");
        }

        public string GetSteamUsername()
        {
            return Steamworks.SteamClient.Name;
        }

        public void OnInvitedByFriend(Steamworks.Friend f, string s)
        {
            Debug.Log("Invited by friend");
            PhotonController.instance.JoinRoom(s);
            SceneManager.LoadScene("MainMenu");
        }

        public void TriggerAchievement(string name)
        {
            if (Steamworks.SteamClient.IsValid)
            {
                Achievement ach = new Achievement(name);
                ach.Trigger();
                MessagePump.instance.QueueMsg(new SteamAchievementTriggeredMessage());
            }
        }

        private void OnApplicationQuit()
        {
            Steamworks.SteamClient.Shutdown();
        }

        IEnumerator InitFromFailure()
        {
            while(!Steamworks.SteamClient.IsValid)
            {
                Steamworks.SteamClient.Init(settings.appID);
                yield return new WaitForSeconds(3);
            }
        }
    }
}
