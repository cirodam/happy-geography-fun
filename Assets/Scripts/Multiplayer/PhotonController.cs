﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace cirodam
{
    public class PhotonController : MonoBehaviourPunCallbacks, IOnEventCallback
    {
        public static PhotonController instance;

        public GameSettings settings;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                StartCoroutine(Init());
            }
            else
            {
                Destroy(gameObject);
            }
        }

        IEnumerator Init()
        {
            //Initialize self
            yield return null;

            //Reigster for messages
            MessagePump.instance.Register(MessageType.PLAYER_KICKED_FROM_ROOM_MESSAGE, OnPlayerKickedFromRoom);
            MessagePump.instance.Register(MessageType.PLAYFAB_SET_DISPLAY_NAME_SUCCESS_MESSAGE, OnPlayFabSetDisplayNameSuccess);
            yield return null;

            //Send messages
            yield return null;
        }

        /// <summary>
        /// MessageListener for PLAYER_KICKED_FROM_ROOM_MESSAGE
        /// </summary>
        /// <param name="msg"></param>
        private void OnPlayerKickedFromRoom(Message msg)
        {
            PlayerKickedFromRoomMessage cMsg = (PlayerKickedFromRoomMessage)msg;

            if(cMsg.playerID == GetID())
            {
                LeaveRoom();
            }
        }

        public void CreateRoom(string name)
        {
            PhotonNetwork.CreateRoom(name, new RoomOptions { MaxPlayers = settings.maxRoomSize, EmptyRoomTtl = 0, PublishUserId = true });
        }

        public object Deserialize(byte[] b)
        {
            if (b == null)
            {
                return null;
            }

            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(b, 0, b.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = binForm.Deserialize(memStream);
                return obj;
            }
        }

        public string GetID()
        {
            if (GetPhotonConnected())
            {
                return PhotonNetwork.LocalPlayer.UserId;
            }
            else
            {
                if (PlayFabController.instance.playerData.playFabID != "")
                {
                    return PlayFabController.instance.playerData.playFabID;
                }
                else
                {
                    return "1";
                }
            }
        }

        public int GetLobbyRoomCount()
        {
            return PhotonNetwork.CountOfRooms;
        }

        public string GetName()
        {
            if (GetPhotonConnected())
            {
                return PhotonNetwork.LocalPlayer.NickName;
            }
            else
            {
                if (PlayFabController.instance.playerData.displayName != "")
                {
                    return PlayFabController.instance.playerData.displayName;
                }
                else
                {
                    return "Player 1";
                }
            }
        }

        public string GetNameFromID(string id)
        {
            foreach (Player p in PhotonNetwork.CurrentRoom.Players.Values)
            {
                if (p.UserId == id)
                {
                    return p.NickName;
                }
            }

            return null;
        }

        public bool GetPhotonConnected()
        {
            return PhotonNetwork.IsConnected;
        }

        public bool GetPhotonReady()
        {
            return PhotonNetwork.IsConnectedAndReady;
        }

        public Dictionary<int, Player> GetPlayersInRoom()
        {
            return PhotonNetwork.CurrentRoom.Players;
        }

        public string GetRoomName()
        {
            return PhotonNetwork.CurrentRoom.Name;
        }

        public int GetRoomPlayerCount()
        {
            return PhotonNetwork.CurrentRoom.PlayerCount;
        }

        public bool InALobby()
        {
            return PhotonNetwork.InLobby;
        }

        public bool InARoom()
        {
            return PhotonNetwork.InRoom;
        }

        public bool IsMasterClient()
        {
            return PhotonNetwork.IsMasterClient;
        }

        public bool IsPlayerMasterClient(Player p)
        {
            return p.IsMasterClient;
        }

        public void JoinLobby()
        {
            PhotonNetwork.JoinLobby();
        }

        public void JoinRoom(string name)
        {
            PhotonNetwork.JoinRoom(name);
        }

        public void LeaveLobby()
        {
            PhotonNetwork.LeaveLobby();
            MessagePump.instance.QueueMsg(new LobbyLeftMessage());
        }

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom(false);
        }

        public override void OnConnected()
        {
            base.OnConnected();
            SetPlayerNickname(PlayFabController.instance.GetDisplayName());
            MessagePump.instance.QueueMsg(new PhotonConnectedMessage());
        }

        public override void OnConnectedToMaster()
        {
            base.OnConnectedToMaster();
            MessagePump.instance.QueueMsg(new PhotonConnectedToMasterMessage());
        }

        public override void OnCreatedRoom()
        {
            base.OnCreatedRoom();
            MessagePump.instance.QueueMsg(new RoomCreatedMessage());
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            base.OnCreateRoomFailed(returnCode, message);
            MessagePump.instance.QueueMsg(new RoomCreationFailedMessage());
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.PLAYER_KICKED_FROM_ROOM_MESSAGE, OnPlayerKickedFromRoom);
            MessagePump.instance.Unregister(MessageType.PLAYFAB_SET_DISPLAY_NAME_SUCCESS_MESSAGE, OnPlayFabSetDisplayNameSuccess);
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            MessagePump.instance.QueueMsg(new PhotonDisconnectedMessage(cause));
        }

        public void OnEvent(EventData photonEvent)
        {

            byte[] data = (byte[])photonEvent.CustomData;

            if (photonEvent.Code < 100)
            {
                MessagePump.instance.QueueFromPhoton((Message)Deserialize(data));
            }
        }

        public override void OnJoinedLobby()
        {
            base.OnJoinedLobby();
            MessagePump.instance.QueueMsg(new LobbyJoinedMessage());
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            MessagePump.instance.QueueMsg(new RoomJoinedMessage());
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            base.OnJoinRoomFailed(returnCode, message);
            Debug.Log("Failed to Join Room");
        }

        public override void OnMasterClientSwitched(Player newMasterClient)
        {
            base.OnMasterClientSwitched(newMasterClient);
            MessagePump.instance.QueueMsg(new RoomMasterClientChangedMessage(newMasterClient));
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
            MessagePump.instance.QueueMsg(new PlayerJoinedRoomMessage(newPlayer));
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            base.OnPlayerLeftRoom(otherPlayer);
            MessagePump.instance.QueueMsg(new PlayerLeftRoomMessage(otherPlayer));
        }

        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            base.OnRoomListUpdate(roomList);
            MessagePump.instance.QueueMsg(new RoomListUpdateMessage(roomList));
        }

        public override void OnLeftRoom()
        {
            base.OnLeftRoom();
            MessagePump.instance.QueueMsg(new RoomLeftMessage());
        }

        private void OnPlayFabSetDisplayNameSuccess(Message msg)
        {
            PlayFabSetDisplayNameSuccessMessage cMsg = (PlayFabSetDisplayNameSuccessMessage)msg;

            SetPlayerNickname(cMsg.displayName);
        }

        public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
        {
            base.OnRoomPropertiesUpdate(propertiesThatChanged);
            foreach (object e in propertiesThatChanged.Keys)
            {
                if (e.GetType() == typeof(string))
                {
                    switch ((string)e)
                    {
                        case "rt":
                            MessagePump.instance.QueueMsg(new RoomTargetChangedMessage((int)propertiesThatChanged["rt"]));
                            break;
                    }
                }
            }
        }

        public void SendMessageToRoom(Message msg)
        {
            if (msg.originalSender == "")
            {
                msg.originalSender = GetID();
                PhotonNetwork.RaiseEvent(1, Serialize(msg), null, SendOptions.SendReliable);
            }
        }

        public byte[] Serialize(object o)
        {
            if (o == null)
            {
                return null;
            }

            var bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, o);
                return ms.ToArray();
            }
        }

        public void SetPlayerNickname(string nickname)
        {
            PhotonNetwork.LocalPlayer.NickName = nickname;
        }

        public void SetRoomPropertiesListedInLobby(string[] props)
        {
            PhotonNetwork.CurrentRoom.SetPropertiesListedInLobby(props);
        }

        public void SetRoomProperty(string key, int value)
        {
            PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { key, value } });
        }

        public void SetRoomProperty(string key, string value)
        {
            PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { key, value } });
        }

        public void SetRoomProperty(string key, byte[] value)
        {
            PhotonNetwork.CurrentRoom.SetCustomProperties(new ExitGames.Client.Photon.Hashtable { { key, value } });
        }
    }
}
