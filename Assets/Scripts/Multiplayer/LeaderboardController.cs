﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using PlayFab;
using PlayFab.ClientModels;

namespace cirodam
{
    [System.Serializable]
    public struct SavedScore
    {
        public string leaderboard;
        public int score;

        public SavedScore(string newLeaderboard, int newScore)
        {
            leaderboard = newLeaderboard;
            score = newScore;
        }
    }

    public class LeaderboardController : MonoBehaviour
    {
        public static LeaderboardController instance;
        public Dictionary<string, int> savedScores;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                StartCoroutine(Init());
            }
            else
            {
                Destroy(gameObject);
            }
        }

        IEnumerator Init()
        {
            //Init self
            savedScores = new Dictionary<string, int>();
            LoadScores();
            yield return null;

            //Register for messages
            yield return null;

            //Send messages
            yield return null;
        }

        public void GetLeaderboardScores(string leaderboardID)
        {
            if(PlayFabClientAPI.IsClientLoggedIn())
            {
                PlayFabClientAPI.GetLeaderboard(new GetLeaderboardRequest { StatisticName = leaderboardID, StartPosition = 0, MaxResultsCount = 100 }, OnGetLeaderboardSuccess, OnGetLeaderboardFailure);
            }
            else
            {
                MessagePump.instance.QueueMsg(new FailedToGetLeaderboardScoresMessage());
            }
        }

        private void OnGetLeaderboardSuccess(GetLeaderboardResult r)
        {
            MessagePump.instance.QueueMsg(new LeaderboardScoresReadyMessage(r.Leaderboard));
        }

        private void OnGetLeaderboardFailure(PlayFabError error)
        {
            MessagePump.instance.QueueMsg(new FailedToGetLeaderboardScoresMessage());
        }

        public void AddScoreToLeaderboard(string leaderboardID, int scoreToAdd)
        {
            if(PlayFabClientAPI.IsClientLoggedIn())
            {
                PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest { Statistics = new List<StatisticUpdate>() { new StatisticUpdate() { StatisticName = leaderboardID, Value = scoreToAdd } } }, OnLeaderboardUpdated, error => Debug.Log(error.GenerateErrorReport()));
            }
        }

        public void OnLeaderboardUpdated(UpdatePlayerStatisticsResult res)
        {
            MessagePump.instance.QueueMsg(new ScoreAddedToLeaderboardMessage());
        }

        private void SaveScores()
        {
            List<SavedScore> toSave = new List<SavedScore>();
            foreach (string s in savedScores.Keys)
            {
                toSave.Add(new SavedScore(s, savedScores[s]));
            }

            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/scores.hgf");
            bf.Serialize(file, toSave);
            file.Close();
        }

        private void LoadScores()
        {
            if (File.Exists(Application.persistentDataPath + "/scores.hgf"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/scores.hgf", FileMode.Open);
                List<SavedScore> toLoad = (List<SavedScore>)bf.Deserialize(file);
                file.Close();

                foreach (SavedScore s in toLoad)
                {
                    if (!savedScores.ContainsKey(s.leaderboard))
                    {
                        savedScores.Add(s.leaderboard, s.score);
                    }
                }
            }
        }
    }
}
