﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ClientData", order = 1)]
    public class ClientData : ScriptableObject
    {
        public string clientID;
        public string clientName;
        public Color clientColor;
        public ClientState clientState;
        public bool spectating;
    }
}
