﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace cirodam
{
    public class GameEntry
    {
        [XmlElement("GameName")]
        public string gameName;
        [XmlElement("GameSource")]
        public string gameSource;
        [XmlElement("LeaderboardID")]
        public string leaderboardID;
        [XmlElement("LeaderboardName")]
        public string leaderboardName;
        [XmlElement("SceneName")]
        public string sceneName;
    }
}
