﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class SessionData 
    {
        public SessionStatus sessionStatus;
        public string sceneName;
        public string dataName;
        public string dataObject;
        public string leaderboardName;
        public string leaderboardID;
        public int numPlayers;
    }
}
