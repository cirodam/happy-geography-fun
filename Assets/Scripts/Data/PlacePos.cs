﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class PlacePos
    {
        [XmlElement("PlaceName")]
        public string placeName;
        [XmlElement("Latitude")]
        public float latitude;
        [XmlElement("Longitude")]
        public float longitude;
        [XmlElement("Category")]
        public string catName;
        [XmlElement("Difficulty")]
        public int difficulty;
        [XmlElement("Hint")]
        public string hint = "";
    }
}
