﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace cirodam
{
    [XmlRoot("GameData")]
    public class GameData
    {
        [XmlArray("GameList"), XmlArrayItem("GameEntry")]
        public GameEntry[] games;

        public static AssetBundle gamedata;

        public static GameData LoadGameData()
        {
            if(gamedata == null)
            {
                gamedata = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "gamedata"));                
            }

            TextAsset txt = gamedata.LoadAsset<TextAsset>("GameData.XML");
            XmlSerializer serializer = new XmlSerializer(typeof(GameData));
            StringReader reader = new StringReader(txt.text);
            GameData data = (GameData)serializer.Deserialize(reader);
            reader.Close();

            gamedata.Unload(true);
            return data;
        }

        public static PlaceData LoadPlaceData(string path)
        {
            if (gamedata == null)
            {
                gamedata = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "gamedata"));
            }

            TextAsset placedata = gamedata.LoadAsset<TextAsset>(path);

            XmlSerializer serializer = new XmlSerializer(typeof(PlaceData));
            StringReader reader = new StringReader(placedata.text);
            PlaceData data = (PlaceData)serializer.Deserialize(reader);
            reader.Close();

            gamedata.Unload(true);
            return data;
        }
    }
}
