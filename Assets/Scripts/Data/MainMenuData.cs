﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/MainMenuData", order = 1)]
    public class MainMenuData : ScriptableObject
    {
        public string startingPanel;
        public RoomInfo roomToJoin;
    }
}
