﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SubjectNerd.Utilities;
using System.Xml.Serialization;
using System.IO;

namespace cirodam
{
    [XmlRoot("PlaceData")]
    public class PlaceData
    {
        [XmlArray("PlaceList"), XmlArrayItem("PlacePos")]
        public PlacePos[] places;
    }
}
