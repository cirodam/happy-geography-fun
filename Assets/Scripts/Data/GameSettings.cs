﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GameSettings", order = 1)]
    public class GameSettings : ScriptableObject
    {
        //Game Settings 
        public uint appID = 1073050;

        //World Settings
        public float earthRadiusUnits = 100f;
        public float earthRadiusKm = 6371f;
        public float earthRadiusMi = 3958.8f;

        public float kmPerUnit = 127.42f;
        public float miPerUnit = 79.176f;

        public float maxDistanceUnits = 314.5f;
        public float maxDistanceKm = 20037.5f;
        public float maxDistanceMi = 12450.5f;
        public DistanceUnit distanceUnit = DistanceUnit.METRIC;

        //Round Settings
        public float roundTime = 7.0f;

        //Level Settings
        public int levelSize = 10;
        public float startingDistpFactor = 0.75f;
        public float startingTimepFactor = 0.45f;
        public float pFactorIncrement = 0.05f;

        //Score Setting
        public float startingScoreDistFactor = 20;
        public float startingScoreTimeFactor = 2;
        public float scoreDistInflationIncrement = 4;
        public float scoreTimeInflationIncrement = 2;

        //Multiplayer Settings
        public byte maxRoomSize = 8;
        public int minRoomSize = 2;
    }
}
