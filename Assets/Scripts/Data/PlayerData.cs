﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace cirodam
{
    [Serializable]
    public class PlayerData
    {
        public string playFabID = "";
        public string displayName = "";
    }
}
