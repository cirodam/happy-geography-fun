﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace cirodam
{
    public class MessagePump : MonoBehaviour
    {
        public static MessagePump instance;

        Dictionary<MessageType, List<MessageListener>> listeners;

        Queue<Message> messageQueue;

        private void Awake()
        {
            if (!instance)
            {
                instance = this;
                listeners = new Dictionary<MessageType, List<MessageListener>>();
                messageQueue = new Queue<Message>();
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Update()
        {
            if (messageQueue.Count > 0)
            {
                PostMsg(messageQueue.Dequeue());
            }
        }

        public void Register(MessageType type, MessageListener listener)
        {
            if (!listeners.ContainsKey(type))
            {
                listeners.Add(type, new List<MessageListener>());
            }

            listeners[type].Add(listener);
        }

        public void Unregister(MessageType type, MessageListener listener)
        {
            if (listeners.ContainsKey(type))
            {
                if (listeners[type].Contains(listener))
                {
                    listeners[type].Remove(listener);
                }
            }
        }

        public void QueueMsg(Message msg)
        {
            msg.queueTime = Time.time;
            messageQueue.Enqueue(msg);
        }

        public void QueueFromPhoton(Message msg)
        {
            messageQueue.Enqueue(msg);
        }

        public void PostMsg(Message msg)
        {
            Debug.Log("Sending " + msg.GetMessageType());

            msg.postTime = 0.0f;
            if (listeners.ContainsKey(msg.GetMessageType()))
            {
                foreach (MessageListener listener in listeners[msg.GetMessageType()])
                {
                    listener(msg);
                }
            }

            if (!msg.localOnly && PhotonController.instance.InARoom())
            {
                PhotonController.instance.SendMessageToRoom(msg);
            }
        }
    }
}
