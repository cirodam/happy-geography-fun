﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public abstract class Message
    {
        public string originalSender = "";
        public bool localOnly;

        public float queueTime = 0.0f;
        public float postTime = 0.0f;

        public abstract MessageType GetMessageType();
    }
}
