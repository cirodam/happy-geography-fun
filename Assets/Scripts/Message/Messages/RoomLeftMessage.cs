﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class RoomLeftMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.ROOM_LEFT_MESSAGE;
        }
    }
}
