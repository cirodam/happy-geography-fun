﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class AutoOrbitFinishedMessage : Message
    {
        public AutoOrbitFinishedMessage()
        {
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.AUTO_ORBIT_FINISHED_MESSAGE;
        }
    }
}
