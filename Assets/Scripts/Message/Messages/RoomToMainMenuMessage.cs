﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class RoomToMainMenuMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.ROOM_TO_MAIN_MENU_MESSAGE;
        }
    }
}
