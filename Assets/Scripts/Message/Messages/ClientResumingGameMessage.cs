﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class ClientResumingGameMessage : Message
    {
        public string clientID;

        public ClientResumingGameMessage(string newClientID)
        {
            clientID = newClientID;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.CLIENT_RESUMING_GAME_MESSAGE;
        }
    }
}
