﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class MouseExitStayMarkerMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.MOUSE_EXIT_STAYMARKER_MESSAGE;
        }
    }
}

