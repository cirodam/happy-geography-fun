﻿using PlayFab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class PlayFabSetDisplayNameFailureMessage : Message
    {
        public PlayFabError error;

        public PlayFabSetDisplayNameFailureMessage(PlayFabError newError)
        {
            localOnly = true;
            error = newError;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.PLAYFAB_SET_DISPLAY_NAME_FAILURE_MESSAGE;
        }
    }
}
