﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class RoomMasterClientChangedMessage : Message
    {
        Player masterClient;

        public RoomMasterClientChangedMessage(Player newMasterClient)
        {
            localOnly = true;
            masterClient = newMasterClient;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.ROOM_MASTER_CLIENT_CHANGED_MESSAGE;
        }
    }
}
