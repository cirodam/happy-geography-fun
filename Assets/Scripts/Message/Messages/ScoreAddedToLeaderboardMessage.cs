﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class ScoreAddedToLeaderboardMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.SCORE_ADDED_TO_LEADERBOARD;
        }
    }
}

