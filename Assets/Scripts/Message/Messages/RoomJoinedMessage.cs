﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class RoomJoinedMessage : Message
    {
        public RoomJoinedMessage()
        {
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.ROOM_JOINED_MESSAGE;
        }
    }
}
