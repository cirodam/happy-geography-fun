﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class RoomCreationFailedMessage : Message
    {
        public RoomCreationFailedMessage()
        {
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.ROOM_CREATION_FAILED_MESSAGE;
        }
    }
}
