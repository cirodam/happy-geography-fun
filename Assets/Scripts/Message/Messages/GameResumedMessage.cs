﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class GameResumedMessage : Message
    {
        public GameResumedMessage()
        {
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.GAME_RESUMED_MESSAGE;
        }
    }
}
