﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class FailedToCreateSteamClientMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.FAILED_TO_CREATE_STEAM_CLIENT_MESSAGE;
        }
    }
}



