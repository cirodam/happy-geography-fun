﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class ClientShowingResultCompleteMessage : Message
    {
        public string clientID;

        public ClientShowingResultCompleteMessage(string newClientID)
        {
            clientID = newClientID;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.CLIENT_SHOWING_RESULT_COMPLETE_MESSAGE;
        }
    }
}
