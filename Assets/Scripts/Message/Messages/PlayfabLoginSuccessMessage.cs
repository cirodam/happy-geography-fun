﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class PlayfabLoginSuccessMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.PLAYFAB_LOGIN_SUCCESS_MESSAGE;
        }
    }
}
