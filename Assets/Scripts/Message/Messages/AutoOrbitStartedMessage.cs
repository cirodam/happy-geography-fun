﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class AutoOrbitStartedMessage : Message
    {
        public AutoOrbitStartedMessage()
        {
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.AUTO_ORBIT_STARTED_MESSAGE;
        }
    }
}
