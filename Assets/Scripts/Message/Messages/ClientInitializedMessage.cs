﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class ClientInitializedMessage : Message
    {
        public string clientID;
        public string clientName;

        public ClientInitializedMessage(string newClientID, string newClientName)
        {
            clientID = newClientID;
            clientName = newClientName;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.CLIENT_INITIALIZED_MESSAGE;
        }
    }
}
