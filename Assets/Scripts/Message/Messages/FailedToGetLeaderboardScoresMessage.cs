﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class FailedToGetLeaderboardScoresMessage : Message
    {
        public FailedToGetLeaderboardScoresMessage()
        {
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.FAILED_TO_GET_LEADERBOARD_SCORES_MESSAGE;
        }
    }
}
