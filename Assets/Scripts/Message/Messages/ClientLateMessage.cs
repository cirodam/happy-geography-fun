﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class ClientLateMessage : Message
    {
        public string clientID;

        public ClientLateMessage(string newClientID)
        {
            clientID = newClientID;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.CLIENT_LATE_MESSAGE;
        }
    }
}