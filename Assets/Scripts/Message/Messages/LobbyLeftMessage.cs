﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class LobbyLeftMessage : Message
    {
        public LobbyLeftMessage()
        {
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.LOBBY_LEFT_MESSAGE;
        }
    }
}
