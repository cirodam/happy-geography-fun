﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class UIButtonClickedMessage : Message
    {
        public int audioSource; //5 or 8

        public UIButtonClickedMessage(int newAS)
        {
            audioSource = newAS;
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.UI_BUTTON_CLICKED_MESSAGE;
        }
    }
}

