﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class RoundStartedMessage : Message
    {
        public int roundNumber;
        public PlacePos targetPlace;

        public RoundStartedMessage(int newRoundNumber, PlacePos newTarget)
        {
            roundNumber = newRoundNumber;
            targetPlace = newTarget;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.ROUND_STARTED_MESSAGE;
        }
    }
}
