﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class MusicVolumeSetMessage : Message
    {
        public float musicVolume;
        public MusicVolumeSetMessage(float newVolume)
        {
            musicVolume = newVolume;
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.MUSIC_VOLUME_SET_MESSAGE;
        }
    }
}

