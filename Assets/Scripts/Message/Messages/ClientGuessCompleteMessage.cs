﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class ClientGuessCompleteMessage : Message
    {
        public string clientID;
        public GuessResult guessResult;

        public ClientGuessCompleteMessage(string newClientID, GuessResult newGuessResult)
        {
            clientID = newClientID;
            guessResult = newGuessResult;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.CLIENT_GUESS_COMPLETE_MESSAGE;
        }
    }
}
