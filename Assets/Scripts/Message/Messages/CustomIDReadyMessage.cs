﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class CustomIDReadyMessage : Message
    {
        public string customID;

        public CustomIDReadyMessage(string newCustomID)
        {
            customID = newCustomID;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.CUSTOM_ID_READY_MESSAGE;
        }
    }
}
