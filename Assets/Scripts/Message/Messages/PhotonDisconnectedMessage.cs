﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class PhotonDisconnectedMessage : Message
    {
        public DisconnectCause cause;

        public PhotonDisconnectedMessage(DisconnectCause newCause)
        {
            cause = newCause;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.PHOTON_DISCONNECTED_MESSAGE;
        }
    }
}
