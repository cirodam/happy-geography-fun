﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class GameStartedMessage : Message
    {

        public override MessageType GetMessageType()
        {
            return MessageType.GAME_STARTED_MESSAGE;
        }
    }
}
