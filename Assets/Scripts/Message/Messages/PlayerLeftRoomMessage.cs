﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class PlayerLeftRoomMessage : Message
    {
        public Player player;

        public PlayerLeftRoomMessage(Player newPlayer)
        {
            localOnly = true;
            player = newPlayer;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.PLAYER_LEFT_ROOM_MESSAGE;
        }
    }
}

