﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class SteamClientCreatedMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.STEAM_CLIENT_CREATED_MESSAGE;
        }
    }
}
