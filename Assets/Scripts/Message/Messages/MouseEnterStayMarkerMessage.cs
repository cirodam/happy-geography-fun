﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class MouseEnterStayMarkerMessage : Message
    {
        public GameObject stayMarker;

        public MouseEnterStayMarkerMessage(GameObject newMarker)
        {
            stayMarker = newMarker;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.MOUSE_ENTER_STAYMARKER_MESSAGE;
        }
    }
}

