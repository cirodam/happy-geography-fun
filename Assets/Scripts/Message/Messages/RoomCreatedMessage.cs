﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class RoomCreatedMessage : Message
    {
        public RoomCreatedMessage()
        {
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.ROOM_CREATED_MESSAGE;
        }
    }
}
