﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class RoomListUpdateMessage : Message
    {
        public List<RoomInfo> rooms;

        public RoomListUpdateMessage(List<RoomInfo> newRooms)
        {
            rooms = newRooms;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.ROOM_LIST_UPDATE_MESSAGE;
        }
    }
}
