﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class PlayerKickedFromRoomMessage : Message
    {
        public string playerID;

        public PlayerKickedFromRoomMessage(string newPlayerID)
        {
            playerID = newPlayerID;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.PLAYER_KICKED_FROM_ROOM_MESSAGE;
        }
    }
}
