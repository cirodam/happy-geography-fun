﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class PlayFabFriendsListReadyMessage : Message
    {

        public List<PlayFab.ClientModels.FriendInfo> friends;

        public PlayFabFriendsListReadyMessage(List<PlayFab.ClientModels.FriendInfo> newFriends)
        {
            localOnly = true;
            friends = newFriends;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.PLAYFAB_FRIENDS_LIST_READY_MESSAGE;
        }
    }
}
