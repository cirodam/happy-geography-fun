﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class ClientSpectatingMessage : Message
    {
        public string clientID;

        public ClientSpectatingMessage(string newClientID)
        {
            localOnly = true;
            clientID = newClientID;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.CLIENT_SPECTATING_MESSAGE;
        }
    }
}
