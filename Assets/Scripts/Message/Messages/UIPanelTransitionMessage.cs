﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class UIPanelTransitionMessage : Message
    {
        public string prevUI;
        public string nextUI;

        public UIPanelTransitionMessage(string newPrevUI, string newNextUI)
        {
            localOnly = true;
            prevUI = newPrevUI;
            nextUI = newNextUI;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.UI_PANEL_TRANSITION_MESSAGE;
        }
    }
}
