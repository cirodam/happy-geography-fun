﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class GamePausedMessage : Message
    {
        public GamePausedMessage()
        {
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.GAME_PAUSED_MESSAGE;
        }
    }
}
