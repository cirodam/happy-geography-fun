﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class ClientPausingGameMessage : Message
    {
        public string clientID;

        public ClientPausingGameMessage(string newClientID)
        {
            clientID = newClientID;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.CLIENT_PAUSING_GAME_MESSAGE;
        }
    }
}