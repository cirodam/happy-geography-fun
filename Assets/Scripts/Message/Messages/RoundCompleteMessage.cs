﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class RoundCompleteMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.ROUND_COMPLETE_MESSAGE;
        }
    }
}
