﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class GuessingCompleteMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.GUESSING_COMPLETE_MESSAGE;
        }
    }
}
