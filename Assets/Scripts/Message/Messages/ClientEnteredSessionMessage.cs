﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class ClientEnteredSessionMessage : Message
    {
        public string clientID;

        public ClientEnteredSessionMessage(string newID)
        {
            clientID = newID;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.CLIENT_ENTERED_SESSION_MESSAGE;
        }
    }
}
