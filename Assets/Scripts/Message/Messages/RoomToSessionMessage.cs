﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class RoomToSessionMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.ROOM_TO_SESSION_MESSAGE;
        }
    }
}
