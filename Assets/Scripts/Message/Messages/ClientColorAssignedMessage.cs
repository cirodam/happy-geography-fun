﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class ClientColorAssignedMessage : Message
    {
        public string clientID;
        public float[] color = new float[4];

        public ClientColorAssignedMessage(string newClientID, Color newColor)
        {
            clientID = newClientID;
            color[0] = newColor.r;
            color[1] = newColor.g;
            color[2] = newColor.b;
            color[3] = newColor.a;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.CLIENT_COLOR_ASSIGNED_MESSAGE;
        }

        public Color getColor()
        {
            return new Color(color[0], color[1], color[2], color[3]);
        }
    }
}
