﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class SoundVolumeSetMessage : Message
    {
        public float soundVolume;

        public SoundVolumeSetMessage(float newVolume)
        {
            soundVolume = newVolume;
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.SOUND_VOLUME_SET_MESSAGE;
        }
    }
}

