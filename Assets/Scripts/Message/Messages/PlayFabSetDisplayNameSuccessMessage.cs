﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class PlayFabSetDisplayNameSuccessMessage : Message
    {
        public string displayName;

        public PlayFabSetDisplayNameSuccessMessage(string newName)
        {
            displayName = newName;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.PLAYFAB_SET_DISPLAY_NAME_SUCCESS_MESSAGE;
        }
    }
}
