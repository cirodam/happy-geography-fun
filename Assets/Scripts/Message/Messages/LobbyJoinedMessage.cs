﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class LobbyJoinedMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.LOBBY_JOINED_MESSAGE;
        }
    }
}
