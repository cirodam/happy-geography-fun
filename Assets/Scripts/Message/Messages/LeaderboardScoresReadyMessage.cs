﻿using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class LeaderboardScoresReadyMessage : Message
    {
        public List<PlayerLeaderboardEntry> scores;

        public LeaderboardScoresReadyMessage(List<PlayerLeaderboardEntry> newScores)
        {
            localOnly = true;
            scores = newScores;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.LEADERBOARD_SCORES_READY_MESSAGE;
        }
    }
}
