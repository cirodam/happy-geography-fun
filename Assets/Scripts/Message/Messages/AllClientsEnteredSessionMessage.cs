﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class AllClientsEnteredSessionMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.ALL_CLIENTS_ENTERED_SESSION_MESSAGE;
        }
    }
}
