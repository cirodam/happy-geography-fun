﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class PhotonConnectedMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.PHOTON_CONNECTED_MESSAGE;
        }
    }
}
