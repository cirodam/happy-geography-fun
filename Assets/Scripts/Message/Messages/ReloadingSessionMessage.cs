﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class ReloadingSessionMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.RELOADING_SESSION_MESSAGE;
        }
    }
}
