﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class PlayerJoinedRoomMessage : Message
    {
        public Player player;

        public PlayerJoinedRoomMessage(Player newPlayer)
        {
            localOnly = true;
            player = newPlayer;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.PLAYER_JOINED_ROOM_MESSAGE;
        }
    }
}
