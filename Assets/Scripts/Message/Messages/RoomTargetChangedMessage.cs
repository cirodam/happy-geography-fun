﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class RoomTargetChangedMessage : Message
    {
        public int target;

        public RoomTargetChangedMessage(int newTarget)
        {
            localOnly = true;
            target = newTarget;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.ROOM_TARGET_CHANGED_MESSAGE;
        }
    }
}
