﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class PlayFabLoginFailureMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.PLAYFAB_LOGIN_FAILURE_MESSAGE;
        }
    }
}
