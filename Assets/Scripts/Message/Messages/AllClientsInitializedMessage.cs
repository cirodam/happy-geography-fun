﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class AllClientsInitializedMessage : Message
    {
        public AllClientsInitializedMessage()
        {
            localOnly = true;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.ALL_CLIENTS_INITIALIZED_MESSAGE;
        }
    }
}
