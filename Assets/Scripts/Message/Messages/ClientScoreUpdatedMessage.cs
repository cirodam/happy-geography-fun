﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class ClientScoreUpdatedMessage : Message
    {
        public string clientID;
        public int score;
        public int deltaScore;

        public ClientScoreUpdatedMessage(string newClientID, int newScore, int newDeltaScore)
        {
            clientID = newClientID;
            score = newScore;
            deltaScore = newDeltaScore;
        }

        public override MessageType GetMessageType()
        {
            return MessageType.CLIENT_SCORE_UPDATED_MESSAGE;
        }
    }
}
