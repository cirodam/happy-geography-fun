﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{

    public class GameModeBeatenMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.GAMEMODE_BEATEN_MESSAGE;
        }
    }
}
