﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class SteamAchievementTriggeredMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.STEAM_ACHIEVEMENT_TRIGGERED_MESSAGE;
        }
    }
}

