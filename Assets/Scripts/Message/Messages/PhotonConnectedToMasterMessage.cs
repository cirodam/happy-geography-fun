﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class PhotonConnectedToMasterMessage : Message
    {
        public override MessageType GetMessageType()
        {
            return MessageType.PHOTON_CONNECTED_TO_MASTER_MESSAGE;
        }
    }
}
