﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class Zoomer : MonoBehaviour
    {

        public float zoomSpeed;

        public float closest;
        public float farthest;

        public Transform holder;

        float position;

        private void Awake()
        {
            position = transform.position.z;
        }

        private void Update()
        {
            float val = Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
            position = Mathf.Clamp((position += val), farthest, closest);

            transform.position = holder.forward * position;
        }
    }
}
