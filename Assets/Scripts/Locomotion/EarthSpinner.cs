﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class EarthSpinner : MonoBehaviour
    {
        public GameObject earth;
        public float earthSpeed;


        private void Update()
        {
            earth.transform.Rotate(new Vector3(0.0f, 0.0f, earthSpeed*Time.deltaTime));
        }
    }
}

