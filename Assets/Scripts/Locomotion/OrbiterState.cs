﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public enum OrbiterState
    {
        PLAYER_CONTROL,
        AUTO_ORBITING,
        PAUSED,
        PAUSED_WHILE_ORBITING,
        FROZEN
    }
}
