﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace cirodam
{
    public class Orbiter : MonoBehaviour
    {
        public UnityEvent AutoOrbitStartedEvent;
        public UnityEvent AutoOrbitEndedEvent;

        OrbiterState state;

        bool orbiting = false;

        float horizRotation = 0;
        float verticalRotation = 0;

        public Transform orbitPoint;

        Quaternion targetOrientation;

        public float autoYOffset = 40;

        private void Awake()
        {
            state = OrbiterState.PLAYER_CONTROL;
        }

        private void Start()
        {
            MessagePump.instance.Register(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
            MessagePump.instance.Register(MessageType.GUESSING_COMPLETE_MESSAGE, OnGuessingComplete);
            MessagePump.instance.Register(MessageType.GAME_PAUSED_MESSAGE, OnGamePaused);
            MessagePump.instance.Register(MessageType.GAME_RESUMED_MESSAGE, OnGameResumed);
        }

        private void Update()
        {
            switch (state)
            {
                case OrbiterState.PLAYER_CONTROL:

                    if (Input.GetMouseButtonDown(0))
                    {
                        orbiting = true;
                    }

                    if (Input.GetMouseButtonUp(0))
                    {
                        orbiting = false;
                    }

                    if (orbiting)
                    {
                        horizRotation += Input.GetAxis("Mouse X");
                        verticalRotation = Mathf.Clamp(verticalRotation - Input.GetAxis("Mouse Y"), -90, 90);
                        orbitPoint.transform.eulerAngles = new Vector3(verticalRotation, horizRotation, 0.0f);
                    }

                    break;

                case OrbiterState.AUTO_ORBITING:

                    orbitPoint.transform.rotation = Quaternion.RotateTowards(orbitPoint.rotation, targetOrientation, 14.0f*Time.deltaTime);

                    if (Vector3.Distance(orbitPoint.eulerAngles, targetOrientation.eulerAngles) < 1)
                    {
                        state = OrbiterState.FROZEN;
                        horizRotation = orbitPoint.transform.eulerAngles.y;
                        verticalRotation = orbitPoint.transform.eulerAngles.x;
                        if (verticalRotation > 90)
                        {
                            verticalRotation = verticalRotation - 360;
                        }
                        MessagePump.instance.QueueMsg(new AutoOrbitFinishedMessage());
                        AutoOrbitEndedEvent.Invoke();
                    }

                    break;
            }
        }

        private void OnRoundStarted(Message msg)
        {
            orbiting = false;
            RoundStartedMessage cMsg = (RoundStartedMessage)msg;
            targetOrientation = Quaternion.Euler(new Vector3(cMsg.targetPlace.latitude, -cMsg.targetPlace.longitude - 90, 0.0f));
            state = OrbiterState.PLAYER_CONTROL;
        }

        private void OnGuessingComplete(Message msg)
        {

            state = OrbiterState.AUTO_ORBITING;
            MessagePump.instance.QueueMsg(new AutoOrbitStartedMessage());
            AutoOrbitStartedEvent.Invoke();
        }

        public void OnGamePaused(Message msg)
        {
            if (state == OrbiterState.AUTO_ORBITING)
            {
                state = OrbiterState.PAUSED_WHILE_ORBITING;
            }
            else
            {
                state = OrbiterState.PAUSED;
            }
        }

        public void OnGameResumed(Message msg)
        {
            if (state == OrbiterState.PAUSED_WHILE_ORBITING)
            {
                state = OrbiterState.AUTO_ORBITING;
            }
            else
            {
                state = OrbiterState.PLAYER_CONTROL;
            }
        }

        private void OnDestroy()
        {
            MessagePump.instance.Unregister(MessageType.ROUND_STARTED_MESSAGE, OnRoundStarted);
            MessagePump.instance.Unregister(MessageType.GUESSING_COMPLETE_MESSAGE, OnGuessingComplete);
            MessagePump.instance.Unregister(MessageType.GAME_PAUSED_MESSAGE, OnGamePaused);
            MessagePump.instance.Unregister(MessageType.GAME_RESUMED_MESSAGE, OnGameResumed);
        }
    }
}
