﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace cirodam
{

    [System.Serializable]
    public class Category
    {
        public string categoryName;

        public float numerator;
        public float denominator;

        public Category(string newCatName)
        {
            categoryName = newCatName;
        }

        public float getValue()
        {
            return numerator / denominator;
        }

        public static List<Category> LoadCategoriesFromFile()
        {
            if (File.Exists(Application.dataPath + "/stats.hgf"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.dataPath + "/stats.hgf", FileMode.Open);
                List<Category> toLoad = (List<Category>)bf.Deserialize(file);
                file.Close();

                return toLoad;
            }

            return new List<Category>();
        }

        public static void SaveCategoriesToFile(List<Category> toSave)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.dataPath + "/stats.hgf");
            bf.Serialize(file, toSave);
            file.Close();
        }

        public static void ClearCategories()
        {
            if (File.Exists(Application.dataPath + "/stats.hgf"))
            {
                File.Delete(Application.dataPath + "/stats.hgf");
            }
        }
    }
}
