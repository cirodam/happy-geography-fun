﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace cirodam
{
    public static class Util
    {

        public static float degreesInRadian = 57.2958f;
        public static float kilometersInMile = 1.60934f;

        //Assumes the spherical coords are in degrees
        public static Vector3 SphericalToCartesian(SCoord sphere)
        {
            Vector3 cart = new Vector3();

            sphere.latitude /= degreesInRadian;
            sphere.longitude /= degreesInRadian;

            float a = sphere.radius * Mathf.Cos(sphere.latitude);

            cart.x = a * Mathf.Cos(sphere.longitude);
            cart.y = sphere.radius * Mathf.Sin(sphere.latitude);
            cart.z = a * Mathf.Sin(sphere.longitude);

            return cart;
        }

        public static SCoord CartesianToSpherical(Vector3 cart)
        {
            SCoord sphere = new SCoord();

            if (cart.x == 0)
                cart.x = Mathf.Epsilon;
            sphere.radius = Mathf.Sqrt((cart.x * cart.x)
                            + (cart.y * cart.y)
                            + (cart.z * cart.z));
            sphere.longitude = Mathf.Atan(cart.z / cart.x);
            if (cart.x < 0)
                sphere.longitude += Mathf.PI;
            sphere.latitude = Mathf.Asin(cart.y / sphere.radius);

            sphere.latitude *= degreesInRadian;
            sphere.longitude *= degreesInRadian;

            return sphere;
        }

        public static float KmToMiles(float km)
        {
            return km / kilometersInMile;
        }

        public static float MilesToKm(float miles)
        {
            return miles * kilometersInMile;
        }

        public static byte[] SerializeToByteArray(object obj)
        {
            if (obj == null)
            {
                return null;
            }
            var bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static object Deserialize(this byte[] byteArray)
        {
            if (byteArray == null)
            {
                return null;
            }
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(byteArray, 0, byteArray.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = binForm.Deserialize(memStream);
                return obj;
            }
        }
    }
}
