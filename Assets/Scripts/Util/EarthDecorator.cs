﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace cirodam
{
    public class EarthDecorator : MonoBehaviour
    {
        public GameSettings settings;
        public Transform center;
        public GameObject markerPrefab;

        PlaceData placeData;
        List<GameObject> markers;

        private void Awake()
        {
            placeData = GameData.LoadPlaceData("Modern Cities.XML");
            markers = new List<GameObject>(); 
            for(int i = 0; i<100; i++)
            {
                PlacePos pos = placeData.places[Random.Range(0, placeData.places.Length - 1)];

                GameObject target = Instantiate(markerPrefab);
                target.transform.SetParent(gameObject.transform);
                target.transform.position = Util.SphericalToCartesian(new SCoord(settings.earthRadiusUnits, pos.latitude, pos.longitude-transform.rotation.y));
                target.transform.LookAt(center);
                target.transform.up = -target.transform.forward;
                markers.Add(target);
            }
        }
    }
}
