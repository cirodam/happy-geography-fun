﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public class GameObjectPool
    {
        Stack<GameObject> objects;
        public GameObject root;
        public int size;

        public GameObjectPool()
        {
            objects = new Stack<GameObject>();
        }

        public void Fill(GameObject prefab, int count)
        {
            for (int i = 0; i < count; i++)
            {
                GameObject add = Object.Instantiate(prefab);
                add.transform.SetParent(root.transform, false);
                add.SetActive(false);
                objects.Push(add);
                size++;
            }
        }

        public void AddToPool(GameObject target)
        {
            size++;
            target.SetActive(false);
            target.transform.SetParent(root.transform, false);
            objects.Push(target);
        }

        public GameObject Use()
        {
            if (objects.Count > 0)
            {
                size--;
                GameObject ret = objects.Pop();
                ret.SetActive(true);
                return ret;
            }

            return null;
        }

        public void Clear()
        {
            for (int i = 0; i < objects.Count; i++)
            {
                Object.Destroy(objects.Pop());
            }
            size = 0;
        }
    }
}
