﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    [System.Serializable]
    public class SVector3
    {
        public float x;
        public float y;
        public float z;

        public SVector3(Vector3 vec)
        {
            x = vec.x;
            y = vec.y;
            z = vec.z;
        }

        public Vector3 toVector()
        {
            return new Vector3(x, y, z);
        }

        public static SVector3 ToSVector3(Vector3 vec)
        {
            return new SVector3(vec);
        }
    }
}
