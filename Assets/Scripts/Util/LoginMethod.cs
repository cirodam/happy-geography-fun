﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{
    public enum LoginMethod
    {
        LOGIN_CUSTOM,
        LOGIN_STEAM,
    }
}
