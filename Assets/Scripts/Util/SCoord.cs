﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{

    public class SCoord
    {
        public float radius;
        public float latitude;
        public float longitude;

        public SCoord()
        {

        }

        public SCoord(float newRadius, float newLat, float newLong)
        {
            radius = newRadius;
            latitude = newLat;
            longitude = newLong;
        }
    }

}
