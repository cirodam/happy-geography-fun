﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cirodam
{

    public enum DistanceUnit
    {
        METRIC,
        IMPERIAL
    }

}
