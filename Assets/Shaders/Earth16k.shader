﻿Shader "Custom/Earth16k"
{
    Properties
    {
		_MainTex0("Albedo (RGB)", 2D) = "white" {}
		_MainTex1("Albedo (RGB)", 2D) = "white" {}
		_MainTex2("Albedo (RGB)", 2D) = "white" {}
		_MainTex3("Albedo (RGB)", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex0;
		sampler2D _MainTex1;
		sampler2D _MainTex2;
		sampler2D _MainTex3;

        struct Input
        {
            float2 uv_MainTex0;
        };
		float2 dbl_uv_MainTex0;

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			dbl_uv_MainTex0 = IN.uv_MainTex0 * 2;

			half4 c0 = tex2D(_MainTex0, dbl_uv_MainTex0 - float2(0.0, 1.0));
			half4 c1 = tex2D(_MainTex1, dbl_uv_MainTex0 - float2(1.0, 1.0));
			half4 c2 = tex2D(_MainTex2, dbl_uv_MainTex0);
			half4 c3 = tex2D(_MainTex3, dbl_uv_MainTex0 - float2(1.0, 0.0));

			if (IN.uv_MainTex0.x >= 0.5)
			{
				if (IN.uv_MainTex0.y <= 0.5)
				{
					c0.rgb = c1.rgb = c2.rgb = 0;
				}
				else
				{
					c0.rgb = c2.rgb = c3.rgb = 0;
				}
			}
			else
			{
				if (IN.uv_MainTex0.y <= 0.5)
				{
					c0.rgb = c1.rgb = c3.rgb = 0;
				}
				else
				{
					c1.rgb = c2.rgb = c3.rgb = 0;
				}
			}

			o.Albedo = c0.rgb + c1.rgb + c2.rgb + c3.rgb;
			o.Alpha = c0.a + c1.a + c2.a + c3.a;
		}
        ENDCG
    }
    FallBack "Diffuse"
}
